// WebServiceC.cpp : Defines the entry point for the console application.
#include "tempuri.org.xsd.h"
#include "tempuri.org.wsdl.h"
#include "schemas.microsoft.com.2003.10.Serialization.xsd.h"
#include <iostream>
#include <stdlib.h>
#include <string>
#include <fstream> 

#include "WebServiceProxy.h"

#include "stdafx.h"

using namespace std;

int _tmain(int argc, _TCHAR* argv[]) {

	int result_login, result_dl, result_ul ;

	// For�ar loggin correcto
	WebServiceProxy * ws = new WebServiceProxy();
	do{
		result_login = ws->login();
	} while (0 != result_login);
	// Fazer download do mapa 1
	result_dl = ws->downloadMap(2);
	// Fazer upload do score do mapa 1
	//result_ul = ws->uploadScore(1000, 1);
	//printf("%d", result_ul);

	return 0;
}

/* Error List:
================
0 - Utilizador ou Password errada
1 -
2 - String do Mapa vazia
3 -
*/


/*
int _tmain(int argc, _TCHAR* argv[])
{
	//=================================================================
	// Webservice & proxy conection code
	//=================================================================
	HRESULT	hr = ERROR_SUCCESS;
	WS_ERROR*	error = NULL;
	WS_HEAP* heap = NULL;
	WS_SERVICE_PROXY* proxy = NULL;
	WS_ENDPOINT_ADDRESS	address = {};	//	endere�o	do	servi�o
	WS_STRING url = WS_STRING_VALUE(L"http://wvm134.dei.isep.ipp.pt/WalkTheMaze/WebService/WebService.svc?wsdl");
	address.url = url;
	hr = WsCreateHeap(65536, 65536, NULL, 0, &heap, error);
	WS_HTTP_BINDING_TEMPLATE		templ = {};
	// cria��o do proxy para o servi�o
	hr = BasicHttpBinding_IWebService_CreateServiceProxy(&templ, NULL, 0, &proxy, error);
	hr = WsOpenServiceProxy(proxy, &address, NULL, error);

	//*****************************************************************
	// Webservice Process code
	//*****************************************************************
	//=======================================
	// Webservice Login code
	//=======================================
	int	   result_id;
	string loginName;
	string password;

	cout << "Login: " << flush;
	getline(std::cin, loginName);
	cout << "Password: " << flush;
	getline(std::cin, password);

	wstring temp1(loginName.begin(), loginName.end());
	wstring temp2(password.begin(), password.end());

	WCHAR * loginToTry = (WCHAR *)temp1.c_str();
	WCHAR * passToTry = (WCHAR *)temp2.c_str();

	//Fazer o pedido
	hr = BasicHttpBinding_IWebService_Login(proxy, loginToTry, passToTry, &result_id, heap, NULL, 0, NULL, error);
	//printf("Welcome %s\n", loginName);
	//return result_id;

	if (0 == result_id){
		//=======================================
		// Webservice Download Map code
		//=======================================
		printf("Downloading Map...\n");
		char* result_dlMap = "";
		WCHAR * wc_result_dlMap;
		int map_id = 2;

		hr = BasicHttpBinding_IWebService_DownloadMap(proxy, loginToTry, passToTry, map_id, &wc_result_dlMap, heap, NULL, 0, NULL, error);
		//wcout << wc_result_dlMap;
		//printf("\n");

		std::wofstream fout;
		fout.open("output.txt");
		fout << wc_result_dlMap;
		fout.close();

		//printf("Downloaded!\n", loginName);
	}
	return 0;
}
*/

