#include "stdafx.h"
#include "WebServiceProxy.h"
#include <fstream> 

//=========================================================================//
// Webservice Constructor with inicialization of the proxy conection code  //
//																		   //
//	Note: Due to a bug, storing the wchar as a class atribute doesn't work //
// because of this, the user and pass are saved as string in the class and //
// reconverted eveythime they are needed...								   //
//=========================================================================//
WebServiceProxy::WebServiceProxy() {
	//------------------
	// Inicialize code
	//------------------
	hr = ERROR_SUCCESS;
	error = NULL;
	heap = NULL;
	proxy = NULL;
	WS_STRING url = WS_STRING_VALUE(L"http://wvm134.dei.isep.ipp.pt/WalkTheMaze/WebService/WebService.svc?wsdl");

	// Atribuir o endere�o do servi�o
	WS_ENDPOINT_ADDRESS address = {}; //endere�o do servi�o
	address.url = url;
	// Defenir o tamanho da Heap para resposta
	hr = WsCreateHeap(65536, 65536, NULL, 0, &heap, error);
	// Definir o template
	WS_HTTP_BINDING_TEMPLATE templ = {};
	// Cria��o do proxy para o servi�o
	hr = BasicHttpBinding_IWebService_CreateServiceProxy(&templ, NULL, 0, &proxy, error);
	// Abrir a proxy
	hr = WsOpenServiceProxy(proxy, &address, NULL, error);
}
WebServiceProxy::~WebServiceProxy()
{}
//===========================================================================//
// Method to try do a successefull login									 //
//	After inputing the user and password, these will be converted to wstrings//
// and sent to the WebService to compare with the DB.						 //
//	Return: 0 = Success; 1 = Failure										 //
//===========================================================================//
int WebServiceProxy::login() {
	int result = -1;

	cout << "Utilizador: " << flush;
	getline(std::cin, loginName);
	cout << "Senha: " << flush;
	getline(std::cin, password);

	wstring temp1(loginName.begin(), loginName.end());
	wstring temp2(password.begin(), password.end());

	WCHAR * loginToTry = (WCHAR *)temp1.c_str();
	WCHAR * passToTry = (WCHAR *)temp2.c_str();

	//Fazer o pedido
	hr = BasicHttpBinding_IWebService_Login(proxy, loginToTry, passToTry, &result, heap, NULL, 0, NULL, error);
	//printf("%d\n", result);

	//se for aceite grava o user e passwd na class
	if (0 == result){
		printf("Sucesso!\n");
	}
	else {
		printf("Utilizador e/ou Senha erradas!\n");
	}
	//para correr sem user definido:
	//return 0;
	return result;
}

//===========================================================================//
// Method to try to Download a map											 //
//	With the given parameters the method will get a long wstring			 //
//	The wstring will be parced to a .txt file for the program to read after  //
// and draw the graph after.												 //
//	Parameters: map_id = id of the map to be downloaded						 //
//	Return: 0																 //
//===========================================================================//
int WebServiceProxy::downloadMap(int map_id) {
	printf("Descarregando Mapa...\n");
	char* result_dlMap = "";
	WCHAR * wc_result_dlMap;

	wstring temp1(loginName.begin(), loginName.end());
	wstring temp2(password.begin(), password.end());

	WCHAR * loginToTry = (WCHAR *)temp1.c_str();
	WCHAR * passToTry = (WCHAR *)temp2.c_str();
	
	// Pedir o Mapa pelo dado id
	hr = BasicHttpBinding_IWebService_DownloadMap(proxy, loginToTry, passToTry, map_id, &wc_result_dlMap, heap, NULL, 0, NULL, error);
	// Re-escrever o fucheiro que l� os grafos com a info nova
	std::wofstream fout;
	fout.open("output.txt");
	fout << wc_result_dlMap;
	fout.close();

	printf("Descarregado!\n");

	return 0;
}

int WebServiceProxy::uploadScore(int score, int map_id) {
	int result = -1;
	wstring temp1(loginName.begin(), loginName.end());
	wstring temp2(password.begin(), password.end());

	WCHAR * loginToTry = (WCHAR *)temp1.c_str();
	WCHAR * passToTry = (WCHAR *)temp2.c_str();

	hr = BasicHttpBinding_IWebService_UploadScore(proxy, loginToTry, passToTry, score, map_id, &result, heap, NULL, 0, NULL, error);
	return result;
}

int WebServiceProxy::uploadPercurso(string percurso, int map_id) {
	int result = -1;
	wstring temp1(loginName.begin(), loginName.end());
	wstring temp2(password.begin(), password.end());

	WCHAR * loginToTry = (WCHAR *)temp1.c_str();
	WCHAR * passToTry = (WCHAR *)temp2.c_str();

	//hr = BasicHttpBinding_IWebService_UploadScore(proxy, loginToTry, passToTry, percurso, map_id, &result, heap, NULL, 0, NULL, error);
	return result;
}


/*
string WebServiceProxy::listMaps() {
	WCHAR * wc_result_listMap;

	wstring temp1(loginName.begin(), loginName.end());
	wstring temp2(password.begin(), password.end());

	WCHAR * loginToTry = (WCHAR *)temp1.c_str();
	WCHAR * passToTry = (WCHAR *)temp2.c_str();
	
	std::wofstream fout;
	fout.open("mapList.txt");
	fout << wc_result_listMap;
	fout.close();
}
