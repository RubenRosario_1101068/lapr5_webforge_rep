#include "No.h"
#include <GL/glut.h>

class Arco {
public:
	Arco();
	Arco(No noInicial, No noFinal, float peso, float largura, GLuint* texArco);
	~Arco();
	No *noInicial;
	No *noFinal;
	float peso;
	float largura;
	GLuint* texArco;
};