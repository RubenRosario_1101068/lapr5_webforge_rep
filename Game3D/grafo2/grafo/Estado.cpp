#include "Estado.h"

Estado::Estado(){

}

void Estado::initEstado(){

	this->camera.eye.x = 0;
	this->camera.eye.y = 0;
	this->camera.eye.z = OBJECTO_ALTURA * 2;
	this->camera.dir_lat = 0;
	this->camera.dir_long = 0;
	this->camera.fov = 60;
	this->camera.dist = 100;

	this->vista = 0;

	this->eixo[0] = 0;
	this->eixo[1] = 0;
	this->eixo[2] = 0;
	this->camera.center[0] = 0;
	this->camera.center[1] = 0;
	this->camera.center[2] = 0;
	this->light = GL_FALSE;
	this->apresentaNormais = GL_FALSE;
	this->lightViewer = 1;
}