#include <GL/glut.h>
#include <AL/alut.h>

#ifndef OBJECTO_ALTURA
#define	OBJECTO_ALTURA		      4
#endif


typedef	GLdouble Vertice[3];
typedef	GLdouble Vector[4];


typedef struct pos_t {
	GLfloat x;
	GLfloat y;
	GLfloat z;
} pos_t;

typedef struct teclas_t{
	GLboolean   up, down, left, right;
}teclas_t;

typedef struct Camera{
	pos_t    eye;
	GLfloat fov;
	GLdouble dir_lat;
	GLdouble dir_long;
	GLfloat dist;
	Vertice center;

}Camera;

class Estado {
public:
	Estado();
	void initEstado();
	GLint         timer;
	Camera		camera;
	teclas_t      teclas;
	int			xMouse, zMouse;
	GLboolean	light;
	ALuint buffer[3], source[3];
	GLboolean	apresentaNormais;
	GLint		lightViewer;
	GLint		eixoTranslaccao;
	GLdouble	eixo[3];
	GLuint      vista;
	GLint       mainWindow, navigateSubwindow;
private:



};