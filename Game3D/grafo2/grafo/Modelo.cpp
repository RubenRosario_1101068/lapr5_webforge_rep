#include "Modelo.h"

Modelo::Modelo(){

}

void Modelo::initModelo(int start_x, int start_y, int start_z){
	this->escala = 0.2;

	this->cor_cubo = brass;
	this->g_pos_luz1[0] = -5.0;
	this->g_pos_luz1[1] = 5.0;
	this->g_pos_luz1[2] = 5.0;
	this->g_pos_luz1[3] = 0.0;
	this->g_pos_luz2[0] = 5.0;
	this->g_pos_luz2[1] = -15.0;
	this->g_pos_luz2[2] = 5.0;
	this->g_pos_luz2[3] = 0.0;

	// Posicao inicial da personagem
	this->objecto.pos.x = start_x;
	this->objecto.pos.y = start_y;
	this->objecto.pos.z = start_z + OBJECTO_ALTURA*0.5;
	this->objecto.dir = 0;
	this->objecto.vel = OBJECTO_VELOCIDADE;
	this->HP = 200;
	this->MaxHP = 200;
	this->pontos = 1000;
	this->timerPontuacao;
	this->percurso = "";
	this->timerPercurso;

}