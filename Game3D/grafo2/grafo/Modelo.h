#include "mathlib.h"
#include "studio.h"
#include <iostream>
#include "mdlviewer.h"
#include "stdafx.h"
#include <GL\glut.h>

using namespace std;

#ifndef OBJECTO_ALTURA
#define	OBJECTO_ALTURA		      4
#endif

#ifndef OBJECTO_VELOCIDADE
#define	OBJECTO_VELOCIDADE		      5
#endif


typedef struct pos {
	GLfloat x;
	GLfloat y;
	GLfloat z;
} pos;

const GLfloat mat_ambient[][4] = { { 0.33, 0.22, 0.03, 1.0 },	// brass
{ 0.0, 0.0, 0.0 },			// red plastic
{ 0.0215, 0.1745, 0.0215 },	// emerald
{ 0.02, 0.02, 0.02 },		// slate
{ 0.0, 0.0, 0.1745 },		// azul
{ 0.02, 0.02, 0.02 },		// preto
{ 0.1745, 0.1745, 0.1745 } };// cinza

const GLfloat mat_diffuse[][4] = { { 0.78, 0.57, 0.11, 1.0 },		// brass
{ 0.5, 0.0, 0.0 },				// red plastic
{ 0.07568, 0.61424, 0.07568 },	// emerald
{ 0.78, 0.78, 0.78 },			// slate
{ 0.0, 0.0, 0.61424 },			// azul
{ 0.08, 0.08, 0.08 },			// preto
{ 0.61424, 0.61424, 0.61424 } };	// cinza

const GLfloat mat_specular[][4] = { { 0.99, 0.91, 0.81, 1.0 },			// brass
{ 0.7, 0.6, 0.6 },					// red plastic
{ 0.633, 0.727811, 0.633 },		// emerald
{ 0.14, 0.14, 0.14 },				// slate
{ 0.0, 0.0, 0.727811 },			// azul
{ 0.03, 0.03, 0.03 },				// preto
{ 0.727811, 0.727811, 0.727811 } };	// cinza

const GLfloat mat_shininess[] = { 27.8,	// brass
32.0,	// red plastic
76.8,	// emerald
18.78,	// slate
30.0,	// azul
75.0,	// preto
60.0 };	// cinza

enum tipo_material { brass, red_plastic, emerald, slate, azul, preto, cinza };

#ifdef __cplusplus
inline tipo_material operator++(tipo_material &rs, int) {
	return rs = (tipo_material)(rs + 1);
}
#endif


typedef struct object_t {
	pos pos;
	GLfloat dir;
	GLfloat vel;
} object_t;


class Modelo {

	
public:
	Modelo();
	void initModelo(int start_x, int start_y, int start_z);
	tipo_material cor_cubo;
	GLfloat g_pos_luz1[4];
	GLfloat g_pos_luz2[4];

	GLfloat escala;
	GLUquadric *quad;
	GLuint prev;
	object_t objecto;
	StudioModel personagem;
	int HP;
	int MaxHP;
	int pontos;
	GLuint* textID;
	GLuint tempo;
	GLuint timerPontuacao;
	string percurso;
	GLuint timerPercurso;
	
private:

};
