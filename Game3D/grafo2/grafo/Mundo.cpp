#include "Mundo.h"
#include <sstream>
#include <iostream>
#include <GL/glut.h>

Mundo::Mundo(){

}

Mundo::Mundo(string stringGrafo, GLuint* texArco, int amundoId){
	mundoId = amundoId;
	int idQuarto = 0, noInicial, noFinal;
	stringstream ss(stringGrafo);
	Quarto& q = *new Quarto();
	//Quarto& q = quartos[idQuarto];
	ss >> numQuartos;
	//numQuartos = 1;
	do{
		//cout << "Quarto " << idQuarto << endl;
		Quarto& q = quartos[idQuarto];
		//q = quartos[idQuarto];
		// Inicializar definini��es do quarto
		ss >> q.numNos;
		for (int i = 0; i < q.numNos; i++) {
			ss >> q.nos[i].posX >> q.nos[i].posY >> q.nos[i].posZ;
			//cout << "x: " << q.nos[i].posX << "y: " << q.nos[i].posY << "z: " << q.nos[i].posZ << endl;
			q.nos[i].id = i;
		}
		ss >> q.numArcos;
		for (int i = 0; i < q.numArcos; i++){
			//ss >> arcos[i].noi >> arcos[i].nof >> arcos[i].peso >> arcos[i].largura;
			ss >> noInicial;
			ss >> noFinal;
			q.arcos[i].noInicial = &q.nos[noInicial];
			q.arcos[i].noFinal = &q.nos[noFinal];
			ss >> q.arcos[i].peso >> q.arcos[i].largura;
			//cout << "peso: " << q.arcos[i].peso << "largura: " << q.arcos[i].largura << endl;
		}

		// calcula a largura de cada no = maior largura dos arcos que divergem/convergem desse/nesse no	
		for (int i = 0; i < q.numNos; i++){
			q.nos[i].largura = 0;
			for (int j = 0; j < q.numArcos; j++){
				if ((q.arcos[j].noInicial->id == i || q.arcos[j].noFinal->id == i) && q.nos[i].largura < q.arcos[j].largura)
					q.nos[i].largura = q.arcos[j].largura;
				q.arcos[j].texArco=texArco;
			}
			
		}

		// guardar as coords iniciais do jogador
		ss >> q.start.x >> q.start.y >> q.start.z;

		// indentificar as saidas 
		ss >> q.numSaidas;
		for (int i = 0; i < q.numSaidas; i++){
			ss >> q.saidas[i].x >> q.saidas[i].y >> q.saidas[i].z >> q.saidas[i].next_quarto;
		}
		// para efeitos de teste...
		// alcapoes
		ss >> q.numAlcapoes;
		for (int i = 0; i < q.numAlcapoes; i++){
			ss >> q.alcapoes[i].x >> q.alcapoes[i].y >> q.alcapoes[i].z ;
		}
		// inimigos
		ss >> q.numInimigos;
		for (int i = 0; i < q.numInimigos; i++){
			ss >> q.inimigios[i].x >> q.inimigios[i].y >> q.inimigios[i].z;
		}

		// Novo quarto?
		// numQuartos++;
		// ...
		
		// Adicionar o novo quarto ao array de quartos e incrementar o indice no array
		idQuarto++;
	} while (idQuarto < numQuartos);
}
//Corrigido para funcionar
void Mundo::desenharLabirinto(StudioModel(&enemyModel)[10], StudioModel(&obstacleModel)[20]){
	//COlocado a 0 para funcionar - variavel quartoActual nao inicializada/alterada
	quartos[quartoActual].desenharQuarto();
	quartos[quartoActual].desenharInimigos(enemyModel);
	if(obstacleModel!=NULL){quartos[quartoActual].desenharObstaculos(obstacleModel);}
	//quartos[0].desenharQuarto();
}

//=============================================================
// Metodo: Dadas as coordenadas da personagem (character), 
//		verifica se este se encontra dentro da area da saida.
//		   A area de saida � um circulo de centro igual as 
//      coordenadas lidas do ficheiro.
//         Formula do circulo: (x-xc)^2+(y-yc)^2=r^2
//         Se: (char_x-xc^)^2 + (char_y-yc) < r^2 
//         Entao: char est� dentro do circlo 
// Tipos de retorno:
//	-2:  N�o "pisou" nenhuma saida;
//  -1: "pisou" a meta final do mundo;
//   i: "pisou" uma porta que liga ao mundo de indice i
// Note: i vai ser >= a 0
//=============================================================
int Mundo::detectaSaida(int char_x, int char_y, int char_z) {
	// precorrer todas as saidas do quarto actual
	float raioQuadrado = pow(2, 2);
	for (int i = 0; i < quartos[quartoActual].numSaidas; i++){
		// verificar se as coordenadas da personagem (character) coincidem com a area da saida.
		int x_centro = quartos[quartoActual].saidas[i].x;
		int y_centro = quartos[quartoActual].saidas[i].y;
		float equacao_x = char_x - x_centro;
		float equacao_y = char_y - y_centro;
		if ((pow(equacao_x, 2) + pow(equacao_y, 2)) < raioQuadrado) {
			return quartos[quartoActual].saidas[i].next_quarto;
		}
	}
	return -2;
}

bool Mundo::detectaAlcapao(int x, int y, int z){
	
	for (int i = 0; i < quartos[quartoActual].numAlcapoes; i++){
		float min_x = quartos[quartoActual].alcapoes[i].x - 4;
		float max_x = quartos[quartoActual].alcapoes[i].x + 4;
		float min_y = quartos[quartoActual].alcapoes[i].y - 4;
		float max_y = quartos[quartoActual].alcapoes[i].y + 4;
		if (x > min_x && x < max_x && y > min_y && y < max_y){

			return true;

		}			

	}
	
	return false;

}

/*
Actualizar variaveis que dependam do tempo
*/

void Mundo::tick(GLuint tempo_passado){
	Quarto &q = quartos[quartoActual];
	for (int i = 0; i < q.numInimigos; i++)
		q.inimigios[i].actualizar_animacoes(tempo_passado);
}


GLboolean Mundo::detectaObstaculo(int char_x, int char_y, int char_z) {
	// precorrer todas as saidas do quarto actual
	for (int i = 0; i < quartos[quartoActual].numObstaculos; i++){
		float d;
		if (quartos[quartoActual].obstaculos[i].largura>quartos[quartoActual].obstaculos[i].comprimento){
			d = quartos[quartoActual].obstaculos[i].largura;
		}
		else{
			d = quartos[quartoActual].obstaculos[i].comprimento;
		}
		float raioQuadrado = pow(d*0.5, 2);

		// verificar se as coordenadas da personagem (character) coincidem com a area da saida.
		int x_centro = quartos[quartoActual].obstaculos[i].x;
		int y_centro = quartos[quartoActual].obstaculos[i].y;
		float equacao_x = char_x - x_centro;
		float equacao_y = char_y - y_centro;
		if ((pow(equacao_x, 2) + pow(equacao_y, 2)) < raioQuadrado) {
			return GL_TRUE;
		}
	}
	return GL_FALSE;
}
