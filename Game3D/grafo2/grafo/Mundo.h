#include <string>
#include "Quarto.h"
#include <GL/glut.h>

using namespace std;

class Mundo {
public:
	int quartoActual = 0;
	int numQuartos;
	Quarto quartos[10];
	int mundoId;

	Mundo();
	Mundo(string stringGrafo, GLuint* texArco, int mundoId);
	void desenharLabirinto(StudioModel(&enemyModel)[10], StudioModel(&obstacleModel)[20]);
	void initMundo(string stringGrafo);
	int detectaSaida(int char_x, int char_y, int char_z);
	bool detectaAlcapao(int x, int y, int z);
	void tick(GLuint tempo_passado);
	GLboolean detectaObstaculo(int char_x, int char_y, int char_z);
};