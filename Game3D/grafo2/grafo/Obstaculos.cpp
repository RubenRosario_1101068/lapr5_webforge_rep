#include "Obstaculos.h"


Obstaculos::Obstaculos(){
	x = 0;
	y = 0;
	z = -1;

	largura=1;
	comprimento=1;
	altura=1;
}

void Obstaculos::draw(StudioModel &sm){


	glPushMatrix();
	//glEnable(GL_TEXTURE_2D);
	glTranslatef(x, y, z + altura*.5);
	glScalef(0.1*largura, 0.1*comprimento, 0.1*altura);
	mdlviewer_display(sm);
	glPopMatrix();
}
