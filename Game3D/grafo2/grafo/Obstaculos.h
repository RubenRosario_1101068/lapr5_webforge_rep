#include <string>
#include <GL\glut.h>
#include "mathlib.h"
#include "studio.h"
#include "mdlviewer.h"

using namespace std;


/*
Estados de obstaculo
*/
class Obstaculos{
public:
	int x;
	int y;
	int z;

	float largura;
	float comprimento;
	float altura;

	Obstaculos();
	void draw(StudioModel &sm);
};