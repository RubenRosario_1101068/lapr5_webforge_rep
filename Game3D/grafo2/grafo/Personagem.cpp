#include "Personagem.h"
#include <stdio.h>

#define TEMPO_MORTE 1423

Personagem::Personagem(){
	HP = 100;
	MaxHP = 100;
	x = 0;
	y = 0;
	z = 0;
	tempo = 0;
	characterState = Char_State::STANDING;
}

Personagem::Personagem(int _HP, int _x, int _y, int _z, string modelPath){
	HP = _HP;
	MaxHP = _HP;
	x = _x;
	y = _y;
	z = _z;
	tempo = 0;
	characterState = Char_State::STANDING;
}

void Personagem::draw(StudioModel &sm){
	if (characterState == Char_State::DYING && sm.GetSequence() != 24)
		sm.SetSequence(24);
	if (characterState == Char_State::DEAD && sm.GetSequence() != 73){
		sm.SetSequence(73);
		z -= 1.95; // o desenho do objecto � feito � volta da origem local do objecto, isto �, uma anima��o que seja
		// "mais alta" em altura que outra, se for desenhada a (0,0,0), vai ter partes em baixo e em cima do 'ch�o'
		// Por outro lado se fizermos essa correc��o, vamos ter que fazer correc��o para outras anima��es
	}


	glPushMatrix();
	glTranslatef(x, y, z+1.5);
	glScalef(SCALE_PERSONAGEM, SCALE_PERSONAGEM, SCALE_PERSONAGEM);
	mdlviewer_display(sm);
	glPopMatrix();

	glPushMatrix();

	drawHPbar();
	glPopMatrix();
}


/*
To draw the billboard we need to make square of the health bar always turned to the camera.
This is explained for example in http://www.lighthouse3d.com/opengl/billboarding/index.php?billCheat
From where we took the actions required to do this, in a simple manner

*/
void Personagem::drawHPbar(){
	float largura = 4;
	float percentHP = HP * 1.0f / MaxHP;
	float modelview[16];
	float green[3] = { 0, 1, 0.0 };
	float red[3] = { 1.0, 0, 0 };
	int i, j;
	glTranslatef(x, y, z + 5);
	glGetFloatv(GL_MODELVIEW_MATRIX, modelview);

	for (i = 0; i < 3; i++){
		for (j = 0; j < 3; j++) {
			if (i == j)
				modelview[i * 4 + j] = 1.0;
			else
				modelview[i * 4 + j] = 0.0;
		}
	}
	glLoadMatrixf(modelview);

	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);


	// Health Bar
	glColor4f(0, 1, 0, 1);

	glBegin(GL_POLYGON);
	glVertex3f(-largura / 2.0, 0, 0);
	glVertex3f((-largura / 2.0) + (largura*percentHP), 0, 0);
	glVertex3f((-largura / 2.0) + (largura*percentHP), 0.5, 0);
	glVertex3f(-largura / 2.0, 0.5, 0);
	glEnd();


	glColor4f(1, 0, 0, 1);
	glBegin(GL_POLYGON);
	glVertex3f((-largura / 2.0) + (largura*percentHP), 0, 0);
	glVertex3f(largura / 2.0, 0, 0);
	glVertex3f(largura / 2.0, 0.5, 0);
	glVertex3f((-largura / 2.0) + (largura*percentHP), 0.5, 0);
	glEnd();

	// border
	glColor4f(0.8, 0.8, 0.8, 1);
	glBegin(GL_LINE_LOOP);
	glVertex3f((-largura / 2.0) - 0.01, 0, 0);
	glVertex3f(largura / 2.0 + 0.01, 0, 0);
	glVertex3f(largura / 2.0, 0.5 + 0.01, 0);
	glVertex3f((-largura / 2.0), 0.5 + 0.01, 0);
	glEnd();
	glEnable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);
}


/*
Iniciar processo de morte de uma personagem, incluindo impor a anima��o de morte
*/
void Personagem::morrer(){
	this->tempo = 0;
	characterState = Char_State::DYING;
}


/*
Actualizar modelos das personagens com tempo passado, para poder determinar quando acaba uma anima��o
*/
void Personagem::actualizar_animacoes(GLuint tempo_passado){
	if (characterState != Char_State::STANDING){
		tempo += tempo_passado;
		if (characterState == Char_State::DYING && tempo >= TEMPO_MORTE){
			characterState = Char_State::DEAD;
			tempo = 0;
		}
	}

}

bool Personagem::estaViva(){
	if (characterState == Char_State::RUNNING || characterState == Char_State::STANDING || characterState == Char_State::WALKING)
		return true;
	return false;
}