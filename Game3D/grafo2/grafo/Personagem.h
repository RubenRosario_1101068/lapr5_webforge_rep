#include <string>
#include <GL\glut.h>
#include "mathlib.h"
#include "studio.h"
#include "mdlviewer.h"

#define SCALE_PERSONAGEM		0.05

using namespace std;

/*
Estados de Personagem
*/

enum class Char_State {
	STANDING,
	WALKING,
	RUNNING,
	DYING,
	DEAD
};


/*
Objecto para representar uma personagem, com posi��o e pontos de vida (Health Points).
� necess�ria guardar tanto o m�ximo de vida (valor inicial), como a vida actual para poder desenhar em percentagem.
*/
class Personagem {
public:
	Personagem();
	Personagem(int _HP, int _x, int _y, int _z, string modelPath);
	int HP;
	int MaxHP;
	int x;
	int y;
	int z;
	GLuint tempo;
	Char_State characterState;
	float dir;
	int state;
	void draw(StudioModel &sm);
	void morrer();
	void actualizar_animacoes(GLuint tempo_passado);
	bool estaViva();
private:
	void drawHPbar();
};