#include "Projectil.h"
#include <math.h>

void Projectil::inicializar(float x, float y, float z, float _dir){
	xActual = x;
	yActual = y;
	zActual = z;
	xOrigem = x;
	yOrigem = y;
	zOrigem = z;
	raio = 1.0f;
	live = true;
	dir = _dir;
	vel = 0.5;

}

void Projectil::actualizar(GLuint tempo){
	xActual = xActual + (vel*tempo*0.1)*cos(-dir);
	yActual = yActual + (vel*tempo*0.1)*sin(-dir);
}

void Projectil::desenhar(){
	GLUquadric *sph = gluNewQuadric();
	glPushMatrix();
	glTranslatef(xActual, yActual, zActual);
	gluSphere(sph, 0.2, 36, 18);
	
	glPopMatrix();
}

bool Projectil::colideInimigo(float x, float y, float z, float raio){
	if (distance(x, y, z) <= raio)
		return true;
	return false;
}

float Projectil::distance(float x, float y, float z){
	float d;
	d = sqrt(pow((x - xActual), 2) + pow((y - yActual), 2));
	return d;
}