#include <GL\glut.h>

class Projectil {
public:
	float xActual, yActual, zActual;
	float xOrigem, yOrigem, zOrigem, raio, vel;
	float dir;
	GLboolean live = false;

	void inicializar(float x, float y, float z, float _dir);
	void actualizar(GLuint tempo);
	void desenhar();
	bool colideInimigo(float x, float y, float z, float raio);
private:
	float distance(float x, float y, float z);
};