#include "Quarto.h"
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <stdlib.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include <math.h>






#define K_CIRCULO 1.0
#define K_LIGACAO 1.0
#define INFINITESIMO 0.001

#define graus(X) (double)((X)*180/M_PI)
#define rad(X)   (double)((X)*M_PI/180)

Quarto::Quarto(){

}

void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[])
{
	glBegin(GL_POLYGON);
	//glTexCoord2f(s, t);
	glVertex3fv(a);
	//glTexCoord2f(s + 0.25, t);
	glVertex3fv(b);
	//glTexCoord2f(s + 0.25, t + 0.25);
	glVertex3fv(c);
	//glTexCoord2f(s, t + 0.25);
	glVertex3fv(d);
	glEnd();
}

void raw_texture_load(const char *filename, GLuint tex[])
{
	char *image;
	int w, h, bpp;

	// allocate a texture name
	glGenTextures(1, tex);

	if (read_JPEG_file(filename, &image, &w, &h, &bpp))
	{
		glBindTexture(GL_TEXTURE_2D, tex[0]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	}
	else{
		printf("Textura %s not Found\n", filename);
		exit(0);
	}

	glBindTexture(GL_TEXTURE_2D, NULL);

}

void Quarto::desenhaAlcapao(){
	GLuint texture[1];
	raw_texture_load("soul_sand.jpg", texture);

	for (int i = 0; i < numAlcapoes; i++){


		glPushMatrix();
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, texture[0]);
		glBegin(GL_QUADS);

		glTexCoord2d(0, 0);
		glVertex3f(((GLfloat)alcapoes[i].x) - 2, ((GLfloat)alcapoes[i].y) + 2, ((GLfloat)0.2));
		glTexCoord2d(1, 0);
		glVertex3f(((GLfloat)alcapoes[i].x) + 2, ((GLfloat)alcapoes[i].y) + 2, ((GLfloat)0.2));
		glTexCoord2d(1, 1);
		glVertex3f(((GLfloat)alcapoes[i].x) + 2, ((GLfloat)alcapoes[i].y) - 2, ((GLfloat)0.2));
		glTexCoord2d(0, 1);
		glVertex3f(((GLfloat)alcapoes[i].x) - 2, ((GLfloat)alcapoes[i].y) - 2, ((GLfloat)0.2));

		glEnd();
		glBindTexture(GL_TEXTURE_2D, NULL);
		glPopMatrix();


	}

}


void desenhaElementoLig(Arco arco){
	// Inicializar nos
	No noi = *arco.noInicial;
	No nof = *arco.noFinal;
	float coords[4][3];
	float raioNo;
	float comprimento;
	float larguraArco = arco.largura;
	float orient = atan2((nof.posY - noi.posY), (nof.posX - noi.posX));

	// Rectangulos horizontais

	// Elementos ligacao No inicial
	raioNo = (K_CIRCULO * noi.largura) / 2.0;
	comprimento = K_LIGACAO * raioNo;
	glPushMatrix();
	glTranslatef(noi.posX, noi.posY, noi.posZ);
	glRotatef(graus(orient), 0, 0, 1);
	glTranslatef(comprimento / 2.0, 0, 0);

	glBegin(GL_POLYGON);


	glTexCoord2d(0,0);
	glVertex3f(-comprimento / 2.0, larguraArco / 2.0, 0);
	glTexCoord2d(1, 0);
	glVertex3f(comprimento / 2.0, larguraArco / 2.0, 0);
	glTexCoord2d(1, 1);
	glVertex3f(comprimento / 2.0, -larguraArco / 2.0, 0);
	glTexCoord2d(0, 1);
	glVertex3f(-comprimento / 2.0, -larguraArco / 2.0, 0);
	glEnd();

	glPopMatrix();

	// Rectangulos Verticais

	glPushMatrix();

	glTranslatef(noi.posX, noi.posY, -INFINITESIMO);
	orient = atan2(nof.posY - noi.posY, nof.posX - noi.posX);
	glRotatef(graus(orient), 0, 0, 1);

	glTranslatef(comprimento / 2.0, 0, 0);


	// 1 rectangulo

	// 1 ponto
	coords[0][0] = -comprimento / 2.0;
	coords[0][1] = -larguraArco / 2.0;
	coords[0][2] = 0.0;

	// 2 ponto
	coords[1][0] = comprimento / 2.0;
	coords[1][1] = -larguraArco / 2.0;
	coords[1][2] = 0.0;

	// 3 ponto
	coords[2][0] = comprimento / 2.0;
	coords[2][1] = -larguraArco / 2.0;
	coords[2][2] = noi.posZ;

	// 4 ponto
	coords[3][0] = -comprimento / 2.0;
	coords[3][1] = -larguraArco / 2.0;
	coords[3][2] = noi.posZ;

	glBegin(GL_POLYGON);

	glTexCoord2f(0, noi.posZ / 10);
	glVertex3fv(coords[3]);
	glTexCoord2f(comprimento / 2.0 / 10, noi.posZ / 10);
	glVertex3fv(coords[2]);
	glTexCoord2f(comprimento / 2.0 / 10, 0);
	glVertex3fv(coords[1]);
	glTexCoord2f(0, 0);
	glVertex3fv(coords[0]);
	glEnd();

	//desenhaPoligonoT(coords[0], coords[1], coords[2], coords[3]);

	// 2 rectangulo

	// 1 ponto
	coords[0][0] = comprimento / 2.0;
	coords[0][1] = larguraArco / 2.0;
	coords[0][2] = 0.0;

	// 2 ponto
	coords[1][0] = -comprimento / 2.0;
	coords[1][1] = larguraArco / 2.0;
	coords[1][2] = 0.0;

	// 3 ponto
	coords[2][0] = -comprimento / 2.0;
	coords[2][1] = larguraArco / 2.0;
	coords[2][2] = noi.posZ;

	// 4 ponto
	coords[3][0] = comprimento / 2.0;
	coords[3][1] = larguraArco / 2.0;
	coords[3][2] = noi.posZ;



	glBegin(GL_POLYGON);
	glTexCoord2f(comprimento / 2.0 / 10, noi.posZ / 10);
	glVertex3fv(coords[3]);
	glTexCoord2f(0, noi.posZ / 10);
	glVertex3fv(coords[2]);
	glTexCoord2f(0, 0);
	glVertex3fv(coords[1]);
	glTexCoord2f(comprimento / 2.0 / 10, 0);
	glVertex3fv(coords[0]);

	glEnd();

	glPopMatrix();

	// No final

	noi = *arco.noFinal;
	nof = *arco.noInicial;

	raioNo = (K_CIRCULO * noi.largura) / 2.0;
	comprimento = K_LIGACAO * raioNo;
	orient = atan2((nof.posY - noi.posY), (nof.posX - noi.posX));
	glPushMatrix();
	glTranslatef(noi.posX, noi.posY, noi.posZ);
	glRotatef(graus(orient), 0, 0, 1);
	glTranslatef(comprimento / 2.0, 0, 0);

	glBegin(GL_POLYGON);

	glTexCoord2d(0, 0);
	glVertex3f(-comprimento / 2.0, larguraArco / 2.0, 0);
	glTexCoord2d(1, 1);
	glVertex3f(comprimento / 2.0, larguraArco / 2.0, 0);
	glTexCoord2d(1, 0);
	glVertex3f(comprimento / 2.0, -larguraArco / 2.0, 0);
	glTexCoord2d(0, 1);
	glVertex3f(-comprimento / 2.0, -larguraArco / 2.0, 0);
	glEnd();


	glPopMatrix();

	// Rectangulos Verticais

	glPushMatrix();

	glTranslatef(noi.posX, noi.posY, -INFINITESIMO);
	orient = atan2(nof.posY - noi.posY, nof.posX - noi.posX);
	glRotatef(graus(orient), 0, 0, 1);

	glTranslatef(comprimento / 2.0, 0, 0);


	// 1 rectangulo

	// 1 ponto
	coords[0][0] = -comprimento / 2.0;
	coords[0][1] = -larguraArco / 2.0;
	coords[0][2] = 0.0;

	// 2 ponto
	coords[1][0] = comprimento / 2.0;
	coords[1][1] = -larguraArco / 2.0;
	coords[1][2] = 0.0;

	// 3 ponto
	coords[2][0] = comprimento / 2.0;
	coords[2][1] = -larguraArco / 2.0;
	coords[2][2] = noi.posZ;

	// 4 ponto
	coords[3][0] = -comprimento / 2.0;
	coords[3][1] = -larguraArco / 2.0;
	coords[3][2] = noi.posZ;

	glBegin(GL_POLYGON);

	glTexCoord2f(0, noi.posZ / 10);
	glVertex3fv(coords[3]);
	glTexCoord2f(comprimento / 2.0 / 10, noi.posZ / 10);
	glVertex3fv(coords[2]);
	glTexCoord2f(comprimento / 2.0 / 10, 0);
	glVertex3fv(coords[1]);
	glTexCoord2f(0, 0);
	glVertex3fv(coords[0]);
	glEnd();
	//desenhaPoligono(coords[0], coords[1], coords[2], coords[3]);

	// 2 rectangulo

	// 1 ponto
	coords[0][0] = comprimento / 2.0;
	coords[0][1] = larguraArco / 2.0;
	coords[0][2] = 0.0;

	// 2 ponto
	coords[1][0] = -comprimento / 2.0;
	coords[1][1] = larguraArco / 2.0;
	coords[1][2] = 0.0;

	// 3 ponto
	coords[2][0] = -comprimento / 2.0;
	coords[2][1] = larguraArco / 2.0;
	coords[2][2] = noi.posZ;

	// 4 ponto
	coords[3][0] = comprimento / 2.0;
	coords[3][1] = larguraArco / 2.0;
	coords[3][2] = noi.posZ;


	glBegin(GL_POLYGON);



	glTexCoord2f(comprimento / 2.0 / 10, noi.posZ / 10);
	glVertex3fv(coords[3]);
	glTexCoord2f(0, noi.posZ / 10);
	glVertex3fv(coords[2]);
	glTexCoord2f(0, 0);
	glVertex3fv(coords[1]);
	glTexCoord2f(comprimento / 2.0 / 10, 0);
	glVertex3fv(coords[0]);

	glEnd();

	glPopMatrix();
}



void desenhaArco(Arco arco){
	desenhaElementoLig(arco);

	No noi = *arco.noInicial;
	No nof = *arco.noFinal;

	float raioi = K_CIRCULO * noi.largura / 2.0;
	float raioj = K_CIRCULO * nof.largura / 2.0;
	float si = K_LIGACAO * raioi;
	float sj = K_LIGACAO * raioj;
	float pij = sqrt(pow(nof.posX - noi.posX, 2) + pow(nof.posY - noi.posY, 2)) - si - sj;
	float alpha = atan2((nof.posY - noi.posY), (nof.posX - noi.posX));
	float hij = nof.posZ - noi.posZ;
	float beta = atan((hij / pij));
	float sij = sqrt(pow(pij, 2) + pow(hij, 2));

	glPushMatrix();
	glTranslatef(noi.posX, noi.posY, noi.posZ);
	glRotatef(graus(alpha), 0, 0, 1);
	glTranslatef(si, 0, 0);
	glRotatef(graus(-beta), 0, 1, 0);
	glTranslatef(sij / 2.0, 0, 0);


	glBegin(GL_POLYGON);


	glTexCoord2f(-sij / 2.0 / 10, -arco.largura / 2.0 / 10);
	glVertex3f(-sij / 2.0, -arco.largura / 2.0, 0);	
	glTexCoord2f(-sij / 2.0 / 10, arco.largura / 2.0 / 10);
	glVertex3f(-sij / 2.0, arco.largura / 2.0, 0);
	glTexCoord2f(sij / 2.0 / 10, arco.largura / 2.0 / 10);
	glVertex3f(sij / 2.0, arco.largura / 2.0, 0);
	glTexCoord2f(sij / 2.0 / 10, -arco.largura / 2.0 / 10);
	glVertex3f(sij / 2.0, -arco.largura / 2.0, 0);

	glEnd();

	glPopMatrix();
	glPushMatrix();
	glTranslatef(noi.posX, noi.posY, -INFINITESIMO);
	glRotatef(graus(alpha), 0, 0, 1);
	glTranslatef(si, 0, 0);


	glBegin(GL_POLYGON);



	glTexCoord2d(0.0, noi.posZ / 10);
	glVertex3f(0, -arco.largura / 2.0, noi.posZ);
	glTexCoord2d(pij / 10, nof.posZ / 10);
	glVertex3f(pij, -arco.largura / 2.0, nof.posZ);
	glTexCoord2d(pij / 10, 0.0);
	glVertex3f(pij, -arco.largura / 2.0, 0);
	glTexCoord2d(0, 0.0);
	glVertex3f(0, -arco.largura / 2.0, 0);
	glEnd();

	glBegin(GL_POLYGON);



	glTexCoord2d(pij / 10, nof.posZ / 10);
	glVertex3f(pij, arco.largura / 2.0, nof.posZ);
	glTexCoord2d(0, noi.posZ / 10);
	glVertex3f(0, arco.largura / 2.0, noi.posZ);
	glTexCoord2d(0, 0);
	glVertex3f(0, arco.largura / 2.0, 0);
	glTexCoord2d(pij / 10, 0);
	glVertex3f(pij, arco.largura / 2.0, 0);
	glEnd();


	glPopMatrix();
}


void Quarto::desenharQuarto(){
	//glEnable(GL_CULL_FACE);
	GLUquadric *disk = gluNewQuadric();
	GLUquadric *quad = gluNewQuadric();
	GLUquadric *saida = gluNewQuadric();
	gluQuadricDrawStyle(disk, GLU_FILL);
	gluQuadricNormals(disk, GLU_OUTSIDE);
	gluQuadricOrientation(disk, GLU_INSIDE);
	gluQuadricTexture(disk, GL_TRUE);

	gluQuadricDrawStyle(quad, GLU_FILL);
	gluQuadricNormals(quad, GL_SMOOTH);
	gluQuadricOrientation(quad, GLU_INSIDE);
	gluQuadricTexture(quad, GL_TRUE);

	gluQuadricDrawStyle(saida, GLU_FILL);
	gluQuadricNormals(saida, GL_SMOOTH);
	gluQuadricOrientation(saida, GLU_INSIDE);
	gluQuadricTexture(saida, GL_FALSE);

	float raioNo;
	int raioSaida;
	glPushMatrix();
	GLuint texture[1];
	raw_texture_load("cimento01.jpg", texture);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	for (int i = 0; i < numNos; i++){
		glPushMatrix();
		raioNo = K_CIRCULO * nos[i].largura / 2.0;

		glPushMatrix();
		glTranslatef(nos[i].posX, nos[i].posY, nos[i].posZ + INFINITESIMO);
		gluDisk(disk, 0, raioNo + INFINITESIMO, 20, 5);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(nos[i].posX, nos[i].posY, -INFINITESIMO);
		gluCylinder(quad, raioNo, raioNo, nos[i].posZ + (2 * INFINITESIMO), 16, 5);
		glPopMatrix();

		glPopMatrix();
	}

	for (int i = 0; i < numArcos; i++)
		desenhaArco(arcos[i]);	
	
	glBindTexture(GL_TEXTURE_2D, NULL);

	// Desenhar um cilindro de cor lemonchiffon Hexa:#FFFACD RBG:(255,250,205) nas saidas
	
	for (int i = 0; i < numSaidas; i++){
		glPushMatrix();
			raioSaida = 2;
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glDisable(GL_LIGHTING);
			//glDisable(GL_DEPTH_TEST);
			glPushMatrix();
				// Activar o blend para o alpha funcionar
				glColor4f(255, 250, 205, 0.5);
				glTranslatef(saidas[i].x, saidas[i].y, -INFINITESIMO);
				gluCylinder(quad, raioSaida, raioSaida, 200, 16, 1);
			glPopMatrix();
			glDisable(GL_BLEND);
			glEnable(GL_LIGHTING);
			//glEnable(GL_DEPTH_TEST);
		glPopMatrix();
	}

	glPopMatrix();

	desenhaAlcapao();

	glPushMatrix();
	glPopMatrix();
}

void Quarto::desenharInimigos(StudioModel(&enemyModel)[10]){
	int i;
	for (i = 0; i < numInimigos; i++){
		glPushMatrix();
		inimigios[i].draw(enemyModel[i]);
		glPopMatrix();
	}
}

void Quarto::desenharObstaculos(StudioModel(&obstacleModel)[20]){
	int i;
	for (i = 0; i < numObstaculos; i++){
		
			glPushMatrix();
			obstaculos[i].draw(obstacleModel[i]);
			glPopMatrix();
	}

}