#define _USE_MATH_DEFINES
#include <stdio.h>
#include <stdlib.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include <math.h>
#include "Arco.h"
#include "Inimigo.h"
#include "Obstaculos.h"

extern "C" int read_JPEG_file(const char *, char **, int *, int *, int *);

class Quarto {

	struct Start{
		int x, y, z;
	};

	struct Saida {
		int x, y, z, next_quarto;
	};
	
	struct Alcapao {
		int x, y, z;
	};	
	

public:
	Start   start;
	No      nos[200];
	Arco    arcos[200];
	Saida   saidas[10];
	Alcapao alcapoes[20];
	Inimigo inimigios[10];
	Obstaculos obstaculos[20];

	int numNos;
	int numArcos;
	//mudan�a de quarto e al�apoes
	int numSaidas;
	int numAlcapoes;
	int numInimigos;
	int numObstaculos;
	
	Quarto();
	void desenharQuarto();
	void desenharInimigos(StudioModel(&enemyModel)[10]);
	void desenhaAlcapao();
	void desenharObstaculos(StudioModel(&obstacleModel)[20]);
};