#include <string>
#include <GL\glut.h>

/*
Excerto para desenho de strings retirado de http://www.academia.edu/3186255/GLUT_Tutorial

*/
void renderBitmapString(float x, float y, float z, void *font, char *string) {
	char *c; glRasterPos3f(x, y, z);
	for (c = string; *c != '\0'; c++) 	{
		glutBitmapCharacter(font, *c);
	}
}