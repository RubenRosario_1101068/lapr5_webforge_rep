﻿#include "tempuri.org.xsd.h"
#include "tempuri.org.wsdl.h"
#include "schemas.microsoft.com.2003.10.Serialization.xsd.h"
#include "WebServiceProxy.h"
#include <fstream> 

#define _USE_MATH_DEFINES
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>     
#include "glut.h"
#include "Estado.h"
#include "Modelo.h"
#include "Mundo.h"
#include "Projectil.h"
#include <AL/alut.h>
#include <iostream>
#include <time.h>
#include <winuser.h>
#include <sstream>


#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#ifdef _WIN32
#include <GL/glaux.h>
#endif

#include "mathlib.h"
#include "Utils.h"

using namespace std;

#pragma comment (lib, "glaux.lib")    /* link with Win32 GLAUX lib usada para ler bitmaps */

// funo para ler jpegs do ficheiro readjpeg.c
extern "C" int read_JPEG_file(const char *, char **, int *, int *, int *);

Projectil projecteis[10];
int indiceProjectil = 0;

StudioModel enemyModel;
StudioModel enemyModels[10];
StudioModel objectModel;
StudioModel obstacleModels[20];

#define graus(X) (double)((X)*180/M_PI)
#define rad(X)   (double)((X)*M_PI/180)

#define LARGURA_CUBO	1
#define ALTURA_CUBO	1
#define K_CIRCULO 1.0
#define K_LIGACAO 1.0
#define OBJECTO_RAIO 0.6

#define SCALE_PERSONAGEM		0.05
#define NUM_JANELAS               2
#define CAM_TOPO                0
#define CAM_FP					1
#define CAM_TP					2
#define EYE_ROTACAO			        1

#define ID_TEXTURA_PAREDES 1
#define ID_TEXTURA_CHAO 2

#define NUM_TEXTURAS 2
#define NOME_TEXTURA_PAREDES "cimento01.bmp"
#define NOME_TEXTURA_CHAO "chao.bmp"

#define LOGIN			0
#define MENU_PRINCIPAL	1
#define JOGO			2
#define MORTE			3
#define FIM_DE_MAPA		4
string text = "You have Died!";
int modoJogo;

struct Mundos{
	int id;
	string nome;
};
Mundos listaMundos[100];
int totalMundos =0;
int dl_mundoId;

struct stringbuilder
{
	std::stringstream ss;
	template<typename T>
	stringbuilder & operator << (const T &data)
	{
		ss << data;
		return *this;
	}
	operator std::string() { return ss.str(); }
};



struct Seta{
	int select;
	int x;
	int y;
	int quadradoLargura;
};
Seta seta;

GLuint        texID[3];

//Mundo, estado, modelo
Mundo *m;
Estado e;

// Janelas

GLint mainWindow, miniMapWindow;

WebServiceProxy * nws;

Estado estado;
Modelo modelo;
bool pisa_alcapao;
string percurso_aux = "", percurso_actual = "";



#ifdef _WIN32

AUX_RGBImageRec *LoadBMP(char *Filename)				// Loads A Bitmap Image
{
	FILE *File = NULL;									// File Handle

	if (!Filename)										// Make Sure A Filename Was Given
	{
		return NULL;									// If Not Return NULL
	}

	File = fopen(Filename, "r");							// Check To See If The File Exists

	if (File)											// Does The File Exist?
	{
		fclose(File);									// Close The Handle
		return auxDIBImageLoad(Filename);				// Load The Bitmap And Return A Pointer
	}

	return NULL;										// If Load Failed Return NULL
}
#endif


void initTexturas(GLuint texID[])
{
	char *image;
	int w, h, bpp;

#ifdef _WIN32
	AUX_RGBImageRec *TextureImage[1];					// Create Storage Space For The Texture

	memset(TextureImage, 0, sizeof(void *)* 1);           	// Set The Pointer To NULL
#endif

	glGenTextures(NUM_TEXTURAS, texID);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);


	if (TextureImage[0] = LoadBMP(NOME_TEXTURA_PAREDES))

	{
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_PAREDES]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);
	}
	else{
		printf("Textura %s not Found\n", NOME_TEXTURA_PAREDES);
		exit(0);
	}
		if (TextureImage[0] = LoadBMP(NOME_TEXTURA_CHAO))

	{
		glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_CHAO]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);
	}
	else{
		printf("Textura %s not Found\n", NULL);
		exit(0);
	}
	glBindTexture(GL_TEXTURE_2D, NULL);
}


void myInit()
{

	GLfloat LuzAmbiente[] = { 0.5, 0.5, 0.5, 0.0 };

	glClearColor(0.0, 0.0, 0.0, 0.0);

	glEnable(GL_SMOOTH); /*enable smooth shading */
	glEnable(GL_LIGHTING); /* enable lighting */
	glEnable(GL_DEPTH_TEST); /* enable z buffer */
	glEnable(GL_NORMALIZE);
	glEnable(GL_TEXTURE_2D);

	//glDepthFunc(GL_LESS);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, LuzAmbiente);
	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, estado.lightViewer);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

	initTexturas(texID);
	modelo.initModelo(m->quartos[0].start.x, m->quartos[0].start.y, m->quartos[0].start.z);
	estado.initEstado();
	modelo.quad = gluNewQuadric();
	gluQuadricDrawStyle(modelo.quad, GLU_FILL);
	gluQuadricNormals(modelo.quad, GLU_OUTSIDE);

	if (glutGetWindow() == mainWindow)
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	else
		glClearColor(1.0f, 1.0f, 1.0f, 0.5f);

	seta.quadradoLargura = 5;
	seta.select = 0;
	seta.x = 400;
	seta.y = 425;
}

void imprime_ajuda(void)
{
	printf("\n\nDesenho de um labirinto a partir de um grafo\n");
	printf("h,H - Ajuda \n");
	printf("i,I - Reset dos Valores \n");
	printf("******* Diversos ******* \n");
	printf("l,L - Alterna o calculo luz entre Z e eye (GL_LIGHT_MODEL_LOCAL_VIEWER)\n");
	printf("k,K - Alerna luz de camera com luz global \n");
	printf("s,S - PolygonMode Fill \n");
	printf("w,W - PolygonMode Wireframe \n");
	printf("p,P - PolygonMode Point \n");
	printf("c,C - Liga/Desliga Cull Face \n");
	printf("n,N - Liga/Desliga apresentaçăo das normais \n");
	printf("r,R - Liga/Desliga efeito chuva \n");
	printf("******* grafos ******* \n");
	printf("F1  - Grava grafo do ficheiro \n");
	printf("F2  - Lę grafo para ficheiro \n");
	printf("F6  - Cria novo grafo\n");
	printf("******* Camera ******* \n");
	printf("Botăo esquerdo - Arrastar os eixos (centro da camera)\n");
	printf("Botăo direito  - Rodar camera\n");
	printf("Botăo direito com CTRL - Zoom-in/out\n");
	printf("PAGE_UP, PAGE_DOWN - Altera distância da camara \n");
	printf("ESC - Sair\n");
}

void material(enum tipo_material mat)
{
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, mat_ambient[mat]);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mat_diffuse[mat]);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_specular[mat]);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, mat_shininess[mat]);
}

const GLfloat red_light[] = { 1.0, 0.0, 0.0, 1.0 };
const GLfloat green_light[] = { 0.0, 1.0, 0.0, 1.0 };
const GLfloat blue_light[] = { 0.0, 0.0, 1.0, 1.0 };
const GLfloat white_light[] = { 1.0, 1.0, 1.0, 1.0 };


void putLights(GLfloat* diffuse)
{
	const GLfloat white_amb[] = { 0.2, 0.2, 0.2, 1.0 };

	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, white_light);
	glLightfv(GL_LIGHT0, GL_AMBIENT, white_amb);
	glLightfv(GL_LIGHT0, GL_POSITION, modelo.g_pos_luz1);

	glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, white_light);
	glLightfv(GL_LIGHT1, GL_AMBIENT, white_amb);
	glLightfv(GL_LIGHT1, GL_POSITION, modelo.g_pos_luz2);

	/* desenhar luz */
	//material(red_plastic);
	//glPushMatrix();
	//	glTranslatef(modelo.g_pos_luz1[0], modelo.g_pos_luz1[1], modelo.g_pos_luz1[2]);
	//	glDisable(GL_LIGHTING);
	//	glColor3f(1.0, 1.0, 1.0);
	//	glutSolidCube(0.1);
	//	glEnable(GL_LIGHTING);
	//glPopMatrix();
	//glPushMatrix();
	//	glTranslatef(modelo.g_pos_luz2[0], modelo.g_pos_luz2[1], modelo.g_pos_luz2[2]);
	//	glDisable(GL_LIGHTING);
	//	glColor3f(1.0, 1.0, 1.0);
	//	glutSolidCube(0.1);
	//	glEnable(GL_LIGHTING);
	//glPopMatrix();

	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
}

/*
Código adaptado das aulas pratico-laboratoriais de SGRAI, do programa Labirinto especificamente.

*/

void desenhaAngVisao(){
	int quartoActual = m->quartoActual;


	GLfloat ratio;
	ratio = (GLfloat)glutGet(GLUT_WINDOW_WIDTH) / glutGet(GLUT_WINDOW_HEIGHT); // proporção 


	glDisable(GL_LIGHTING);
	glDisable(GL_COLOR_MATERIAL);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);


	glPushMatrix();
	glTranslatef(modelo.objecto.pos.x, modelo.objecto.pos.y, -10);
	glColor4f(0, 0, 1, 0.5);
	glRotatef(graus(-modelo.objecto.dir), 0, 0, 1);

	glBegin(GL_TRIANGLES);
	glVertex3f(0, 0, 0);
	glVertex3f(60 * cos(rad(60 * ratio * 0.5)), -15 * sin(rad(60 * ratio * 0.5)), -10);
	glVertex3f(60 * cos(rad(60 * ratio * 0.5)), 15 * sin(rad(60 * ratio * 0.5)), -10);
	GLUquadric *sph = gluNewQuadric();
	glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	gluSphere(sph, 8, 36, 18);
	glEnd();
	glPopMatrix();



}


void desenhaSolo(){
#define STEP 10
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, texID[ID_TEXTURA_CHAO]);
	glBegin(GL_QUADS);
	glNormal3f(0, 0, 1);
	for (int i = -300; i < 300; i += STEP)
	for (int j = -300; j < 300; j += STEP){
				glTexCoord2f(0,0);
				glVertex2f(i,j);
				glTexCoord2f(0,1);
				glVertex2f(i+STEP,j);
				glTexCoord2f(1,1);
				glVertex2f(i+STEP,j+STEP);
				glTexCoord2f(1,0);
				glVertex2f(i,j+STEP);
	}
	glEnd();
	
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, NULL);
}

void CrossProduct(GLdouble v1[], GLdouble v2[], GLdouble cross[])
{
	cross[0] = v1[1] * v2[2] - v1[2] * v2[1];
	cross[1] = v1[2] * v2[0] - v1[0] * v2[2];
	cross[2] = v1[0] * v2[1] - v1[1] * v2[0];
}

GLdouble VectorNormalize(GLdouble v[])
{
	int		i;
	GLdouble	length;

	if (fabs(v[1] - 0.000215956) < 0.0001)
		i = 1;

	length = 0;
	for (i = 0; i < 3; i++)
		length += v[i] * v[i];
	length = sqrt(length);
	if (length == 0)
		return 0;

	for (i = 0; i < 3; i++)
		v[i] /= length;

	return length;
}

void desenhaNormal(GLdouble x, GLdouble y, GLdouble z, GLdouble normal[], tipo_material mat){

	switch (mat){
	case red_plastic:
		glColor3f(1, 0, 0);
		break;
	case azul:
		glColor3f(0, 0, 1);
		break;
	case emerald:
		glColor3f(0, 1, 0);
		break;
	default:
		glColor3f(1, 1, 0);
	}
	glDisable(GL_LIGHTING);
	glPushMatrix();
	glTranslated(x, y, z);
	glScaled(0.4, 0.4, 0.4);
	glBegin(GL_LINES);
	glVertex3d(0, 0, 0);
	glVertex3dv(normal);
	glEnd();
	glPopMatrix();
	glEnable(GL_LIGHTING);
}

GLboolean detetaColisao(GLfloat nx, GLfloat ny, GLfloat nz)
{

	float x = nx;
	float y = ny;
	float z = nz;




	int quartoactual = m->quartoActual;
	Quarto quarto = m->quartos[quartoactual];
	No *noi, *nof;
	Arco *arco = new Arco;
	//nos
	GLfloat z_prox = 0;
	if (m->detectaObstaculo(x,y,z)){
		return GL_TRUE;
	}


	for (int i = 0; i < quarto.numNos; i++){
		if ((x >(quarto.nos[i].posX - (quarto.nos[i].largura*0.5)) && x < (quarto.nos[i].posX + (quarto.nos[i].largura*0.5)))
			&& (y >(quarto.nos[i].posY - (quarto.nos[i].largura*0.5)) && y < (quarto.nos[i].posY + (quarto.nos[i].largura*0.5)))){
			if (z >(quarto.nos[i].posZ - 0.5*OBJECTO_ALTURA)){
				if ((quarto.nos[i].posZ + 1) < 0){
					modelo.objecto.pos.z = (quarto.nos[i].posZ) + OBJECTO_ALTURA / 2 + 0.01;
				}
				else{
					modelo.objecto.pos.z = (quarto.nos[i].posZ) + OBJECTO_ALTURA / 2 + 0.01;
				}

			}
			else{
				return GL_TRUE;
			}
			z_prox = quarto.nos[i].posZ;
		}
	}
	//arcos
	for (int i = 0; i < quarto.numArcos; i++){
		*arco = quarto.arcos[i];
		
		if (arco->noInicial->posX == arco->noFinal->posX){  // arco vertical
			if (arco->noInicial->posY < arco->noFinal->posY){
				noi = arco->noInicial;
				nof = arco->noFinal;
			}
			else{
				nof = arco->noInicial;
				noi = arco->noFinal;
			}

			if ((x < noi->posX + 0.5*nof->largura) && (x > nof->posX - 0.5*nof->largura)
				&& (y > noi->posY) && (y < nof->posY)){
				//verificacao das rampas
				if (noi->posZ == nof->posZ){
					if (z < (nof->posZ + 0.1)){
						return GL_TRUE;
					}
				}
				float difY;
				float difZ;
				if (noi->posZ > nof->posZ){
					difY = (nof->posY - noi->posY);
					difZ = ((nof->posZ) - (noi->posZ));
				}
				else{
					difY = (noi->posY - nof->posY);
					difZ = ((noi->posZ) - (nof->posZ));
				}

					float dec = difZ / difY;
					float encontra1 = -dec * (noi->posY) + (noi->posZ + 1 + 0.5*noi->largura);
					float encontra2 = -dec * (noi->posY) + (noi->posZ - 0.5*noi->largura);
					float limite1 = dec * y + encontra1 + 0.1;
					float limite2 = dec * y + encontra2 + 0.1;
					float meio = (limite2 + limite1) / 2;
					printf("VP ");
					if (z > meio && meio > 0.5){
						modelo.objecto.pos.z = meio + OBJECTO_ALTURA / 2;
					}
					else{
						if ((z > noi->posZ + 0.1) && (z < noi->posZ + 0.1) && (z < limite1 - 0.5) && (z > limite2 + .05)){
							if (meio >= 0.5){
								if (meio - z < 0.5){
									modelo.objecto.pos.z = meio + OBJECTO_ALTURA / 2;
								}
							}
						}
						else{
							if (z < (meio - 0.5)){
								return GL_TRUE;
							}
						}
					}

				
			}
		}
		else{
			if (arco->noInicial->posY == arco->noFinal->posY){ //Arco horizontal						
				if (arco->noInicial->posX < arco->noFinal->posX){
					noi = arco->noInicial;
					nof = arco->noFinal;
				}
				else{
					nof = arco->noInicial;
					noi = arco->noFinal;
				}

				if ((x > noi->posX + 0.5*noi->largura) && (x < nof->posX - 0.5*nof->largura)
					&& (y > noi->posY - 0.5*noi->largura) && (y < nof->posY + 0.5*nof->largura)){

					float difX;
					float difZ;
					//verificacao das rampas
					if (noi->posZ == nof->posZ){
						if (z < (nof->posZ + 0.1)){
							return GL_TRUE;
						}
					}
					if (noi->posZ > nof->posZ){
						difX = nof->posX - noi->posX;
						difZ = nof->posZ - noi->posZ;
					}else
					{
						difX = noi->posX - nof->posX;
						difZ = noi->posZ - nof->posZ;
					}
					float dec = difZ / difX;
					float encontra1 = -dec * (noi->posX) + (noi->posZ + 0.5*noi->largura);
					float encontra2 = -dec * (noi->posX) + (noi->posZ - 0.5*noi->largura);

					float limite1 = dec * x + encontra1;
					float limite2 = dec * x + encontra2;
					float meio = (limite2 + limite1) / 2;
					printf("Horizontal ");
					if (z > meio && meio > 0.5){
						modelo.objecto.pos.z = meio + OBJECTO_ALTURA / 2;
					}
					else{
						if ((z > noi->posZ + 0.1) && (z < nof->posZ + 0.1) && (z < limite1 - 0.5) && (z > limite2 + 0.5)){
							if (meio >= 0.5){
								if ((meio - z) < 0.5){
									modelo.objecto.pos.z = meio + OBJECTO_ALTURA / 2;
								}
							}
						}
						else{
							if (z < nof->posZ){
								return GL_TRUE;
							}
						}
					}
					
				}
			}
			else{ //Arco diagonal
				if ((arco->noInicial->posY < arco->noFinal->posY && arco->noInicial->posX < arco->noFinal->posX)
					|| (arco->noInicial->posY > arco->noFinal->posY && arco->noInicial->posX > arco->noFinal->posX)){
					if (arco->noInicial->posX < arco->noFinal->posX){
						noi = arco->noInicial;
						nof = arco->noFinal;
					}
					else{
						nof = arco->noInicial;
						noi = arco->noFinal;
					}

					float difX = abs((noi->posX - nof->posX));
					float difY = abs((noi->posY - nof->posY));
					float dec = difY / difX;
					float encontra1 = -dec * (noi->posX - 0.5*noi->largura) + (noi->posY + 0.5*noi->largura);
					float encontra2 = -dec * (noi->posX + 0.5*noi->largura) + (noi->posY - 0.5*noi->largura);
					if ((y > noi->posY && y < nof->posY) &&
						(y < (dec * x + encontra1)) && ((y >(dec * x + encontra2)))
						&& (z< (.5*OBJECTO_ALTURA) + noi->posZ) && (z<(.5*OBJECTO_ALTURA) + nof->posZ)){
						return GL_TRUE;
					}
				}
				else{
					if (arco->noInicial->posX < arco->noFinal->posX){
						nof = arco->noInicial;
						noi = arco->noFinal;
					}
					else{
						noi = arco->noInicial;
						nof = arco->noFinal;
					}

					float difX = abs(noi->posX - nof->posX);
					float difY = abs(noi->posY - nof->posY);
					float difZ = abs(noi->posZ - nof->posZ);
					float dec = difY / difX;
					float encontra1 = dec * (noi->posX - 0.5*noi->largura) + (noi->posY - 0.5*noi->largura);
					float encontra2 = dec * (noi->posX + 0.5*noi->largura) + (noi->posY + 0.5*noi->largura);
					float fy = y * (-1);
					if ((y > noi->posY && y < nof->posY) &&	((fy < (dec * x - encontra1)) && (fy >(dec * x - encontra2))) && (z< (.5*OBJECTO_ALTURA) + noi->posZ) && (z<(.5*OBJECTO_ALTURA) + nof->posZ)){
						return GL_TRUE;
					}
					else {
						if ((y > noi->posY && y < nof->posY) && ((fy < (dec * x - encontra1)) && (fy >(dec * x - encontra2))) && (noi->posZ != nof->posZ)){
							float difX = (noi->posX - nof->posX);
							float difY = (noi->posY - nof->posY);
							float difZ = (noi->posZ - nof->posZ);
							float encontra2, encontra1;
							if (noi->posZ < nof->posZ){
								dec = difZ / difX;
								encontra1 = -dec * (noi->posX) + (noi->posZ + 0.5*noi->largura);
								encontra2 = -dec * (noi->posX) + (noi->posZ - 0.5*noi->largura);
							}else{
								dec = difZ / difX;
								encontra1 = -dec * (nof->posX) + (nof->posZ + 0.5*noi->largura);
								encontra2 = -dec * (nof->posX) + (nof->posZ - 0.5*noi->largura);
							}
							float dec = difZ / difX;
							float limite1 = dec * x + encontra1 + 0.5;
							float limite2 = dec * x + encontra2 + 0.5;
							float meio = (limite2 + limite1) / 2;
							printf("DP");
							if (z > meio && meio > 0.5){
								modelo.objecto.pos.z = meio + OBJECTO_ALTURA / 2;
							}
							else{
								if ((z > noi->posZ + 0.1) && (z < nof->posZ + 0.1) && (z < limite1 - 0.5) && (z > limite2 + 0.5)){
									if (meio >= 0){
										if (meio - z < 0.5){
											modelo.objecto.pos.z = meio + OBJECTO_ALTURA / 2;
										}
									}
								}
								else{
									if (z < (meio - 0.5)){
										return GL_TRUE;
									}
								}
							}
						}
					}
				}//fecho else rampas diagonal 
			}//fecho else diagonal
			
		}//fecho else arcos nao verticais
	}

	/*queda em nós*/
	if (z - (.5*OBJECTO_ALTURA) > z_prox + arco->largura){
			z = z_prox + arco->largura + (.5*OBJECTO_ALTURA);
	}
	

	return GL_FALSE;
}

bool interseccaoCirculos(int x1, int y1, float r1, int x2, int y2,  float r2 ){
	float diffr = pow(abs(r1 - r2), 2);
	float sumr = pow(r1 + r2, 2);
	float circle = pow((x1 - x2),2) + pow((y1 - y2), 2);

	if (diffr <= circle && circle <= sumr){
		return true;
	}

	return false;
}

int colisaoInimigos(){
	int x, y;

	x = modelo.objecto.pos.x;
	y = modelo.objecto.pos.y;

	Quarto q = m->quartos[m->quartoActual];
	for (int i = 0; i < q.numInimigos; i++){
		if (interseccaoCirculos(x, y, OBJECTO_RAIO, q.inimigios[i].x, q.inimigios[i].y, 1) && q.inimigios[i].HP >0){
			return i;
		}
	}

	return -1;


}

void movimentoProjecteis(GLuint tempo){	
	Quarto &q = m->quartos[m->quartoActual];
	for (int i = 0; i < 10; i++){
		if (projecteis[i].live){
			projecteis[i].actualizar(tempo);
			for (int j = 0; j < q.numInimigos; j++){
				if (q.inimigios[j].estaViva()){
					if (projecteis[i].colideInimigo(q.inimigios[j].x, q.inimigios[j].y, q.inimigios[j].z, 1)){
						printf("colide");
						q.inimigios[j].HP = 0;
						q.inimigios[j].morrer();
						projecteis[i].live = false;
						modelo.pontos += 50;
						PlaySound("sounds/Homer - Oopsy2", NULL, SND_ASYNC | SND_FILENAME);
					}
				}
			}
		}
	}


}

void Timer(int value){
	GLfloat nx = 0, ny = 0, nz = 0;
	boolean blocked = false;
	GLboolean andar = GL_FALSE;
	GLboolean colisao = GL_FALSE;
	GLuint curr = glutGet(GLUT_ELAPSED_TIME);

	modelo.tempo += curr - modelo.prev;
	modelo.timerPontuacao += curr - modelo.prev;
	modelo.timerPercurso += curr - modelo.prev;

	if (modelo.timerPercurso > 5000){

		percurso_actual = stringbuilder() << modelo.objecto.pos.x << ";" << modelo.objecto.pos.y << ";" << modelo.objecto.pos.z << "\n";

		if (percurso_actual != percurso_aux){

			modelo.timerPercurso = 0;
			modelo.percurso += percurso_actual;
			percurso_aux = stringbuilder() << modelo.objecto.pos.x << ";" << modelo.objecto.pos.y << ";" << modelo.objecto.pos.z << "\n";

		}

	}

	if (modelo.timerPontuacao > 10000){
		modelo.timerPontuacao = 0;
		modelo.pontos -= 20;

	}

	// calcula velocidade baseado no tempo passado
	float velocidade = modelo.objecto.vel*(curr - modelo.prev)*0.001;

	m->tick(curr - modelo.prev);
	movimentoProjecteis(curr - modelo.prev);


	glutTimerFunc(estado.timer, Timer, 0);
	modelo.prev = curr;

	if (modelo.personagem.GetSequence() == 20){
		if (modelo.tempo > 1166){
			printf("tempo: %d\n", modelo.tempo);
			modelo.personagem.SetSequence(0);
			modelo.tempo = 0;
		}
		else {
			blocked = true;
		}
	}

	ALint state;

	alGetSourcei(estado.source[0], AL_SOURCE_STATE, &state);
	if (state != AL_PLAYING)
		alSourcePlay(estado.source[0]);

	if (estado.teclas.up && !blocked){
		// calcula nova posiçăo nx,ny
		nx = modelo.objecto.pos.x + velocidade*cos(-modelo.objecto.dir);
		ny = modelo.objecto.pos.y + velocidade*sin(-modelo.objecto.dir);
		nz = modelo.objecto.pos.z;
		if (!detetaColisao((nx + OBJECTO_RAIO*cos(-modelo.objecto.dir)), ny - OBJECTO_RAIO * sin(modelo.objecto.dir), nz)
			&& !detetaColisao((nx + OBJECTO_RAIO*cos(-modelo.objecto.dir - M_PI / 4)), (ny + OBJECTO_RAIO*sin(-modelo.objecto.dir - M_PI / 4)), nz)
			&& !detetaColisao((nx + OBJECTO_RAIO*cos(-modelo.objecto.dir + M_PI / 4)), (ny + OBJECTO_RAIO*sin(-modelo.objecto.dir + M_PI / 4)), nz)){
			modelo.objecto.pos.x = nx;
			//cout << nx << endl;
			modelo.objecto.pos.y = ny;
			andar = GL_TRUE;

			alGetSourcei(estado.source[1], AL_SOURCE_STATE, &state);
			if (state != AL_PLAYING)
				alSourcePlay(estado.source[1]);
		}
		else{
			//PlaySound("Homer - D'oh! (1)", NULL, SND_ASYNC | SND_FILENAME);

		}

	}
	if (estado.teclas.down && !blocked){
		nx = modelo.objecto.pos.x - velocidade*cos(-modelo.objecto.dir);
		ny = modelo.objecto.pos.y - velocidade*sin(-modelo.objecto.dir);
		// calcula nova posiçăo nx,nz
		if (!detetaColisao((nx + OBJECTO_RAIO*cos(-modelo.objecto.dir)), ny - OBJECTO_RAIO * sin(modelo.objecto.dir), nz) && !detetaColisao((nx + OBJECTO_RAIO*cos(-modelo.objecto.dir - M_PI / 4)), (ny + OBJECTO_RAIO*sin(-modelo.objecto.dir - M_PI / 4)), nz) && !detetaColisao((nx + OBJECTO_RAIO*cos(-modelo.objecto.dir + M_PI / 4)), (ny + OBJECTO_RAIO*sin(-modelo.objecto.dir + M_PI / 4)), modelo.objecto.pos.z)){
			modelo.objecto.pos.x = nx;
			modelo.objecto.pos.y = ny;

			alGetSourcei(estado.source[1], AL_SOURCE_STATE, &state);
			if (state != AL_PLAYING)
				alSourcePlay(estado.source[1]);
		}
		else{
			//PlaySound("Homer - Collapsed lung", NULL, SND_ASYNC | SND_FILENAME);
		}
		//int prox_mapa = m->detectaSaida(modelo.objecto.pos.x, modelo.objecto.pos.y, modelo.objecto.pos.z);
		andar = GL_TRUE;
	}


	if (estado.teclas.left && !blocked){
		modelo.objecto.dir -= rad(1.5);
		estado.camera.dir_long -= rad(1.5);
	}
	if (estado.teclas.right && !blocked){
		modelo.objecto.dir += rad(1.5);
		estado.camera.dir_long += rad(1.5);
		// rodar camara e objecto
	}

	//------------------------------------
	//Algoritomo para mudar de quartos
	//------------------------------------
	int coords_x_spawn;
	int coords_y_spawn;
	int coords_z_spawn;
	int prox_mapa = m->detectaSaida(modelo.objecto.pos.x, modelo.objecto.pos.y, modelo.objecto.pos.z);
	//cout << "o prox mapa é" << prox_mapa << endl;
	if (-1 == prox_mapa){
		cout << "Congratz, you finnished the game! :)";
		nws->uploadScore(modelo.pontos, m->mundoId);
		nws->uploadPercurso(modelo.percurso, m->mundoId);
		//modoJogo = FIM_DE_MAPA;
		exit(0);
	}
	else if (0 <= prox_mapa){
		//para todos as as saidas do prox_mapa, 
		// se a saida.next_quarto == quartoActual
		// x = saida.x, y = saida.y, z = saida.z
		for (int i = 0; i < m->quartos[prox_mapa].numSaidas; i++){
			if (m->quartos[prox_mapa].saidas[i].next_quarto == m->quartoActual){
				coords_x_spawn = m->quartos[prox_mapa].saidas[i].x;
				coords_y_spawn = m->quartos[prox_mapa].saidas[i].y;
				coords_z_spawn = m->quartos[prox_mapa].saidas[i].z;
			}
		}

		m->quartoActual = prox_mapa;

		modelo.objecto.pos.x = coords_x_spawn + 3;
		modelo.objecto.pos.y = coords_y_spawn + 3;
		modelo.objecto.pos.z = coords_z_spawn + (OBJECTO_ALTURA / 2);
	}

	if (pisa_alcapao == false){

		if (m->detectaAlcapao(modelo.objecto.pos.x, modelo.objecto.pos.y, modelo.objecto.pos.z) == true){
			modoJogo = MORTE;
		}
	}
	//esta em cima do alcapao
	else{
		//saiu de cima do alcapao
		if (m->detectaAlcapao(modelo.objecto.pos.x, modelo.objecto.pos.y, modelo.objecto.pos.z) == false){
			modoJogo = JOGO;
			pisa_alcapao = false;
		}
	}

	int colide = -1;
	colide = colisaoInimigos();
	if (colide >= 0){ // Colidiu com inimigo com indice i
		colisao = GL_TRUE;
		Quarto &q = m->quartos[m->quartoActual];
		q.inimigios[colide].HP = 0;
		q.inimigios[colide].morrer();
		modelo.pontos -= 100;
		modelo.HP -= 50;
		PlaySound("sounds/Homer - D'oh! (2)", NULL, SND_ASYNC | SND_FILENAME);
	}

	if (andar && modelo.personagem.GetSequence() != 12){
		modelo.personagem.SetSequence(12);
	}
	if (!andar && modelo.personagem.GetSequence() == 12){
		modelo.personagem.SetSequence(0);
	}
	if (colisao == GL_TRUE && modelo.personagem.GetSequence() != 20){
		modelo.personagem.SetSequence(20);
		modelo.tempo = 0;
	}

	/*ALint state;
		alGetSourcei(estadoSND.source, AL_SOURCE_STATE, &state);
		if (estadoSND.tecla_s)
		{
		if (state != AL_PLAYING)
		alSourcePlay(estadoSND.source);
		}
		else
		if (state == AL_PLAYING)
		alSourceStop(estadoSND.source);*/
	// Sequencias - 0(parado) 3(andar) 20(choque)
	//  modelo.homer[JANELA_NAVIGATE].GetSequence()  le Sequencia usada pelo homer
	//  modelo.homer[JANELA_NAVIGATE].SetSequence()  muda Sequencia usada pelo homer
	glutPostRedisplay();
}

void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLfloat normal[])
{
	glBegin(GL_POLYGON);
	glNormal3fv(normal);
	//glTexCoord2f(s, t);
	glVertex3fv(a);
	//glTexCoord2f(s + 0.25, t);
	glVertex3fv(b);
	//glTexCoord2f(s + 0.25, t + 0.25);
	glVertex3fv(c);
	//glTexCoord2f(s, t + 0.25);
	glVertex3fv(d);
	glEnd();
}

void desenhaCubo(){

	GLfloat vertices[][3] = { { -0.5, -0.5, -0.5 },
	{ 0.5, -0.5, -0.5 },
	{ 0.5, 0.5, -0.5 },
	{ -0.5, 0.5, -0.5 },
	{ -0.5, -0.5, 0.5 },
	{ 0.5, -0.5, 0.5 },
	{ 0.5, 0.5, 0.5 },
	{ -0.5, 0.5, 0.5 } };
	GLfloat normais[][3] = { { 0, 0, -1 },
		// acrescentar as outras normais...
	{ 0, 1, 0 },
	{ -1, 0, 0 },
	{ 1, 0, 0 },
	{ 0, 0, 1 },
	{ 0, -1, 0 }
	};


	//glBindTexture(GL_TEXTURE_2D, id);

	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0]);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[1]);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[2]);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[3]);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[4]);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[5]);

	//glBindTexture(GL_TEXTURE_2D, NULL);
}

void desenhaEixo(){
	gluCylinder(modelo.quad, 0.5, 0.5, 20, 16, 15);
	glPushMatrix();
	glTranslatef(0, 0, 20);
	glPushMatrix();
	glRotatef(180, 0, 1, 0);
	gluDisk(modelo.quad, 0.5, 2, 16, 6);
	glPopMatrix();
	gluCylinder(modelo.quad, 2, 0, 5, 16, 15);
	glPopMatrix();
}

#define EIXO_X		1
#define EIXO_Y		2
#define EIXO_Z		3

void desenhaPlanoDrag(int eixo){
	glPushMatrix();
	glTranslated(estado.eixo[0], estado.eixo[1], estado.eixo[2]);
	switch (eixo) {
	case EIXO_Y:
		if (abs(estado.camera.dir_lat) < M_PI / 4)
			glRotatef(-90, 0, 0, 1);
		else
			glRotatef(90, 1, 0, 0);
		material(red_plastic);
		break;
	case EIXO_X:
		if (abs(estado.camera.dir_lat) > M_PI / 6)
			glRotatef(90, 1, 0, 0);
		material(azul);
		break;
	case EIXO_Z:
		if (abs(cos(estado.camera.dir_long)) > 0.5)
			glRotatef(90, 0, 0, 1);

		material(emerald);
		break;
	}
	glBegin(GL_QUADS);
	glNormal3f(0, 1, 0);
	glVertex3f(-100, 0, -100);
	glVertex3f(100, 0, -100);
	glVertex3f(100, 0, 100);
	glVertex3f(-100, 0, 100);
	glEnd();
	glPopMatrix();
}

void desenhaEixos(){

	glPushMatrix();
	glTranslated(estado.eixo[0], estado.eixo[1], estado.eixo[2]);
	material(emerald);
	glPushName(EIXO_Z);
	desenhaEixo();
	glPopName();
	glPushName(EIXO_Y);
	glPushMatrix();
	glRotatef(-90, 1, 0, 0);
	material(red_plastic);
	desenhaEixo();
	glPopMatrix();
	glPopName();
	glPushName(EIXO_X);
	glPushMatrix();
	glRotatef(90, 0, 1, 0);
	material(azul);
	desenhaEixo();
	glPopMatrix();
	glPopName();
	glPopMatrix();
}

void desenhaInterface(){
	string stringHP;
	string stringPontos;
	stringHP = "HP ";
	stringHP.append(to_string(modelo.HP));
	stringHP.append("/");
	stringHP.append(to_string(modelo.MaxHP));
	stringPontos = "Pontos ";
	stringPontos.append(to_string(modelo.pontos));
	/*
	Inicializar matrizes para desenho em 2D
	*/
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0.0, glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT), 0.0, -1.0, 10.0);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);
	glClear(GL_DEPTH_BUFFER_BIT);

	glColor4f(1, 1, 1, 1);

	renderBitmapString(5, 30, 0, GLUT_BITMAP_TIMES_ROMAN_24, (char*)stringHP.c_str());
	renderBitmapString(5, glutGet(GLUT_WINDOW_HEIGHT) - 10, 0, GLUT_BITMAP_TIMES_ROMAN_24, (char*)stringPontos.c_str());

	// Repor matrizes de projecção
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);
}


void setCamera(Camera cam, object_t obj){
	pos_t center;

	if (estado.vista==CAM_FP)
	{
		cam.eye.x = obj.pos.x;
		cam.eye.y = obj.pos.y;
		cam.eye.z = obj.pos.z+0.5*OBJECTO_ALTURA;

		center.x = obj.pos.x + cos(cam.dir_long) *cos(cam.dir_lat);
		center.y = obj.pos.y + sin(-cam.dir_long) *cos(cam.dir_lat);
		center.z = cam.eye.z;
		gluLookAt(cam.eye.x, cam.eye.y, cam.eye.z, center.x, center.y, center.z, 0, 0, 1);
	}
	else if(estado.vista==CAM_TOPO)
	{
		center.x = modelo.objecto.pos.x;
		center.y = modelo.objecto.pos.y;
		center.z = 0;
		cam.eye.x = center.x;
		cam.eye.z = 50;
		cam.eye.y = center.y;
		gluLookAt(cam.eye.x, cam.eye.y, cam.eye.z, center.x, center.y, center.z, 0, 1, 0);
	}
	else if (estado.vista == CAM_TP)
	{
		center.x = obj.pos.x;
		center.y = obj.pos.y;
		center.z = obj.pos.z + 0.75*OBJECTO_ALTURA;

		cam.eye.x = center.x - cos(cam.dir_long)*cos(cam.dir_lat)*(OBJECTO_ALTURA*2.5); 
		cam.eye.y = center.y - sin(-cam.dir_long)*cos(cam.dir_lat)*(OBJECTO_ALTURA*2.5);
		cam.eye.z = center.z + (0.25*OBJECTO_ALTURA);
		gluLookAt(cam.eye.x, cam.eye.y, cam.eye.z, center.x, center.y, center.z, 0, 0, 1);
	}
	else{
		//em caso de ser outro valor restabelece o default
		estado.vista = CAM_TOPO;
	}

	
	putLights((GLfloat*)white_light);

}

void drawText(const char *text, int length, int x, int y){
	glMatrixMode(GL_PROJECTION);
	double *matrix = new double[16];
	glGetDoublev(GL_PROJECTION_MATRIX, matrix);
	glLoadIdentity();
	glOrtho(0, 800, 0, 600, -5, 5);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
	glLoadIdentity();
	glRasterPos2i(x, y);
	for (int i = 0; i<length; i++){
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, (int)text[i]);
	}
	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixd(matrix);
	glMatrixMode(GL_MODELVIEW);
}


void drawMapSelection(){

	glMatrixMode(GL_PROJECTION);
	double *matrix = new double[16];
	glGetDoublev(GL_PROJECTION_MATRIX, matrix);
	glLoadIdentity();
	glOrtho(0, 800, 0, 600, -5, 5);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
	glLoadIdentity();
	int i=seta.select;
	int f = i + 5;
	int oldi = i;
	for (i; i<f; i++){

		glRasterPos2i(200, 425 - (i - oldi) * 50);
		string text = listaMundos[i].nome;
		const char *textX = text.data();
		for (int j = 0; j<text.size(); i++){
			glutBitmapCharacter(GLUT_BITMAP_9_BY_15, (int)textX[j]);
		}
	}

	glPopMatrix();

	glPushMatrix();
	glRasterPos2i(seta.x, seta.y);

	glBegin(GL_POLYGON);
	glVertex2f(seta.x - seta.quadradoLargura*.5, seta.y - seta.quadradoLargura*.5);
	glVertex2f(seta.x + seta.quadradoLargura*.5, seta.y - seta.quadradoLargura*.5);
	glVertex2f(seta.x + seta.quadradoLargura*.5, seta.y + seta.quadradoLargura*.5);
	glVertex2f(seta.x - seta.quadradoLargura*.5, seta.y + seta.quadradoLargura*.5);
	glEnd();
	glBegin(GL_POLYGON);
	glVertex2f(seta.x - seta.quadradoLargura*.5, seta.y + seta.quadradoLargura*.5 + 5);
	glVertex2f(seta.x - seta.quadradoLargura, seta.y);
	glVertex2f(seta.x - seta.quadradoLargura*.5, seta.y - seta.quadradoLargura*.5 - 5);


	glEnd();


	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixd(matrix);
	glMatrixMode(GL_MODELVIEW);
}

void display(void)
{
	GLfloat ambient[] = { 0.2f, 0.2f, 0.2f, 1.0f }; //default material has this ambient color!
	GLfloat diffuse[] = { 0.8f, 0.8f, 0.8f, 1.0f }; //default material has this diffuse color!
	GLfloat specular[] = { 0.0, 0.0, 0.0, 1.0 }; //default specular
	GLfloat shininess = 0;
	
	switch (modoJogo)
	{
	case LOGIN:// Display da janela para fazer loging
		
		break;
	case MENU_PRINCIPAL:// Display da janela para escolher mapa

		/*glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glDisable(GL_LIGHTING);
		gluLookAt(0, 0, -10, 0, 0, 3, 0, 1, 0);
		glColor3f(1, 0, 0);
		glBegin(GL_LINES);
		glVertex3f(0, 0, 0);
		glVertex3f(1, 0, 0);
		glEnd();
		drawMapSelection();
		glFlush();
		glutSwapBuffers();
		glEnable(GL_LIGHTING);*/
		//cin.get();
		break;
	case JOGO:
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();
		setCamera(estado.camera, modelo.objecto);

		desenhaSolo();

		//material(emerald);
		m->desenharLabirinto(enemyModels,obstacleModels);

		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient); //restore default material ambient color
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse); //restore default material diffuse color
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
		glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess);

		glPushMatrix();
		glTranslatef(modelo.objecto.pos.x, modelo.objecto.pos.y, modelo.objecto.pos.z);
		glRotatef(-graus(modelo.objecto.dir), 0, 0, 1);
		glScalef(SCALE_PERSONAGEM, SCALE_PERSONAGEM, SCALE_PERSONAGEM);
		mdlviewer_display(modelo.personagem);
		glPopMatrix();

		for (int i = 0; i < 10; i++){
			if (projecteis[i].live){
				projecteis[i].desenhar();
			}
		}

		desenhaInterface();
		glFlush();
		glutSwapBuffers();
		break;
	case MORTE: // Display da janela de morte
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glDisable(GL_LIGHTING);
		gluLookAt(0, 0, -10, 0, 0, 3, 0, 1, 0);
		glColor3f(1, 0, 0);
		glBegin(GL_LINES);
		glVertex3f(0, 0, 0);
		glVertex3f(1, 0, 0);
		glEnd();
		drawText(text.data(), text.size(), 330, 310);		
		glFlush();
		glutSwapBuffers();
		glEnable(GL_LIGHTING);
		//cin.get();
		m->quartoActual = 0;
		modelo.objecto.pos.x = m->quartos[m->quartoActual].start.x; // mudar para posições dinamicas
		modelo.objecto.pos.y = m->quartos[m->quartoActual].start.y; // mudar para posições dinamicas
		
		pisa_alcapao = false;
		glutPostRedisplay();
		break;
	default:
		break;
	}

}

void drawMinimapSubwindow() {
	glutSetWindow(miniMapWindow);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	int width = glutGet(GLUT_WINDOW_WIDTH);
	int height = glutGet(GLUT_WINDOW_HEIGHT);
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_DEPTH_TEST);
	glPushMatrix();
	glLoadIdentity();

	gluLookAt(modelo.objecto.pos.x, modelo.objecto.pos.y, 120, modelo.objecto.pos.x, modelo.objecto.pos.y, 0, 0, 1, 0); // vista de cima
	putLights((GLfloat*)white_light);
	material(emerald);
	glColor4f(0.18, 0.38, 0.43, 1);
	m->desenharLabirinto(enemyModels, obstacleModels);
	desenhaAngVisao();

	glPopMatrix();
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_LIGHTING);


	glViewport(0, 0, width, height);
	glutSwapBuffers();
}

void rainEffect()
{
	ALint state;

	alGetSourcei(estado.source[2], AL_SOURCE_STATE, &state);
	if (state != AL_PLAYING)
		alSourcePlay(estado.source[2]);
	else if (state == AL_PLAYING)
		alSourceStop(estado.source[2]);
}

void keyboard(unsigned char key, int x, int y)
{
	switch (modoJogo)
	{
	case JOGO:
		switch (key)
		{
		case 32:
			projecteis[indiceProjectil].inicializar(modelo.objecto.pos.x,
				modelo.objecto.pos.y,
				modelo.objecto.pos.z,
				modelo.objecto.dir);
			indiceProjectil = (indiceProjectil + 1) % 10;
			PlaySound("sounds/Gun Silencer", NULL, SND_ASYNC | SND_FILENAME);
			break;

		case 27:
			exit(0);
			break;
		case 'h':
		case 'H':
			imprime_ajuda();
			break;
		case 'l':
		case 'L':
			if (estado.lightViewer)
				estado.lightViewer = 0;
			else
				estado.lightViewer = 1;
			glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, estado.lightViewer);
			glutPostRedisplay();
			break;
		case 'k':
		case 'K':
			estado.light = !estado.light;
			glutPostRedisplay();
			break;
		case 'w':
		case 'W':
			glDisable(GL_LIGHTING);
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			glutPostRedisplay();
			break;
		case 'p':
		case 'P':
			glDisable(GL_LIGHTING);
			glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
			glutPostRedisplay();
			break;
		case 's':
		case 'S':
			glEnable(GL_LIGHTING);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glutPostRedisplay();
			break;
		case 'c':
		case 'C':
			if (glIsEnabled(GL_CULL_FACE))
				glDisable(GL_CULL_FACE);
			else
				glEnable(GL_CULL_FACE);
			glutPostRedisplay();
			break;
		case 'n':
		case 'N':
			estado.apresentaNormais = !estado.apresentaNormais;
			glutPostRedisplay();
			break;
		case 'i':
		case 'I':
			modoJogo = JOGO;
			estado.initEstado();
			m->quartoActual = 0;
			modelo.initModelo(m->quartos[0].start.x, m->quartos[0].start.y, m->quartos[0].start.z);
			glutPostRedisplay();
			break;
		case 'v':
		case 'V':
			estado.vista++;
			break;
		case 'x':
			modelo.objecto.vel += 2;
			break;
		case 'X':
			modelo.objecto.vel -= 2;
			break;
		case 'r':
		case 'R':
			rainEffect();
			break;
		}
		break;
	case MORTE:
		switch (key)
		{
		default:
			modoJogo = JOGO;
			break;
		}
	default:
		break;
	}
		
}

void Special(int key, int x, int y){

	switch (key){
	case GLUT_KEY_F1:
		//gravaGrafo();
		break;
	case GLUT_KEY_F2:
		//leGrafo();
		glutPostRedisplay();
		break;

	case GLUT_KEY_F6:
		//numNos = numArcos = 0;
		//addNo(criaNo(0, 10, 0));  // 0
		//addNo(criaNo(0, 5, 0));  // 1
		//addNo(criaNo(-5, 5, 0));  // 2
		//addNo(criaNo(5, 5, 0));  // 3
		//addNo(criaNo(-5, 0, 0));  // 4
		//addNo(criaNo(5, 0, 0));  // 5
		//addNo(criaNo(-5, -5, 0));  // 6
		//addArco(criaArco(0, 1, 1, 1));  // 0 - 1
		//addArco(criaArco(1, 2, 1, 1));  // 1 - 2
		//addArco(criaArco(1, 3, 1, 1));  // 1 - 3
		//addArco(criaArco(2, 4, 1, 1));  // 2 - 4
		//addArco(criaArco(3, 5, 1, 1));  // 3 - 5
		//addArco(criaArco(4, 5, 1, 1));  // 4 - 5
		//addArco(criaArco(4, 6, 1, 1));  // 4 - 6
		glutPostRedisplay();
		break;
	case GLUT_KEY_UP:
		estado.teclas.up = true;
		//estado.camera.dist -= 1;
		//glutPostRedisplay();
		break;
	case GLUT_KEY_DOWN:
		estado.teclas.down = GL_TRUE;
		//estado.camera.dist += 1;
		//glutPostRedisplay();
		break;
	case GLUT_KEY_LEFT: estado.teclas.left = GL_TRUE;
		break;
	case GLUT_KEY_RIGHT: estado.teclas.right = GL_TRUE;
		break;
	}
}

void SpecialKeyUp(int key, int x, int y)
{
	switch (key) {
	case GLUT_KEY_UP: estado.teclas.up = GL_FALSE;
		break;
	case GLUT_KEY_DOWN: estado.teclas.down = GL_FALSE;
		break;
	case GLUT_KEY_LEFT: estado.teclas.left = GL_FALSE;
		break;
	case GLUT_KEY_RIGHT: estado.teclas.right = GL_FALSE;
		break;
	}
}


void setProjection(int x, int y, GLboolean picking){
	glLoadIdentity();
	if (picking) { // se está no modo picking, lę viewport e define zona de picking
		GLint vport[4];
		glGetIntegerv(GL_VIEWPORT, vport);
		gluPickMatrix(x, glutGet(GLUT_WINDOW_HEIGHT) - y, 4, 4, vport); // Inverte o y do rato para corresponder ŕ jana
	}

	gluPerspective(estado.camera.fov, (GLfloat)glutGet(GLUT_WINDOW_WIDTH) / glutGet(GLUT_WINDOW_HEIGHT), 1, 500);

}

void myReshape(int w, int h){
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	setProjection(0, 0, GL_FALSE);
	glMatrixMode(GL_MODELVIEW);

	glutSetWindow(miniMapWindow);
	glutPositionWindow(w*0.75, 10);
	glutReshapeWindow(w*0.25 - 10, h*0.25);

}

void reshapeMinimapSubwindow(int width, int height){
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	setProjection(0, 0, GL_FALSE);
	glMatrixMode(GL_MODELVIEW);
}


void motionRotate(int x, int z){
#define DRAG_SCALE	0.01
	double lim = M_PI / 2 - 0.1;
	int dif;
	dif = z - estado.zMouse;
	if (dif>0){//olhar para baixo
		estado.camera.dir_lat -= dif*rad(EYE_ROTACAO);
		if (estado.camera.dir_lat<-rad(45))
			estado.camera.dir_lat += -rad(45);
	}

	if (dif<0){//olhar para cima
		estado.camera.dir_lat += abs(dif)*rad(EYE_ROTACAO);
		if (estado.camera.dir_lat>rad(45))
			estado.camera.dir_lat -= rad(45);
	}

	dif = x - estado.xMouse;
	if (dif>0){ //olhar para a direita
		estado.camera.dir_long -= dif*rad(EYE_ROTACAO);
		/*
		if(estado.camera.dir_long<modelo.objecto.dir-RAD(45))
		estado.camera.dir_long=modelo.objecto.dir-RAD(45);
		*/
	}
	if (dif<0){//olhar para a esquerda
		estado.camera.dir_long += abs(dif)*rad(EYE_ROTACAO);
		/*
		if(estado.camera.dir_long>modelo.objecto.dir+RAD(45))
		estado.camera.dir_long=modelo.objecto.dir+RAD(45);
		*/

	}
	estado.xMouse = x;
	estado.zMouse = z;
	glutPostRedisplay();
}

void motionZoom(int x, int z){
#define ZOOM_SCALE	0.5
	estado.camera.dist -= (estado.zMouse - z)*ZOOM_SCALE;
	if (estado.camera.dist < 5)
		estado.camera.dist = 5;
	else
	if (estado.camera.dist > 200)
		estado.camera.dist = 200;
	estado.zMouse = z;
	glutPostRedisplay();
}

void motionDrag(int x, int y){
	GLuint buffer[100];
	GLint vp[4];
	GLdouble proj[16], mv[16];
	int n;
	GLdouble newx, newy, newz;

	glSelectBuffer(100, buffer);
	glRenderMode(GL_SELECT);
	glInitNames();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix(); // guarda a projecçăo
	glLoadIdentity();
	setProjection(x, y, GL_TRUE);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	setCamera(estado.camera, modelo.objecto);
	desenhaPlanoDrag(estado.eixoTranslaccao);

	n = glRenderMode(GL_RENDER);
	if (n > 0) {
		glGetIntegerv(GL_VIEWPORT, vp);
		glGetDoublev(GL_PROJECTION_MATRIX, proj);
		glGetDoublev(GL_MODELVIEW_MATRIX, mv);
		gluUnProject(x, glutGet(GLUT_WINDOW_HEIGHT) - y, (double)buffer[2] / UINT_MAX, mv, proj, vp, &newx, &newy, &newz);
		printf("Novo x:%lf, y:%lf, z:%lf\n\n", newx, newy, newz);
		switch (estado.eixoTranslaccao) {
		case EIXO_X:
			estado.eixo[0] = newx;
			//estado.eixo[1]=newy;
			break;
		case EIXO_Y:
			estado.eixo[1] = newy;
			//estado.eixo[2]=newz;
			break;
		case EIXO_Z:
			//estado.eixo[0]=newx;
			estado.eixo[2] = newz;
			break;
		}
		glutPostRedisplay();
	}


	glMatrixMode(GL_PROJECTION); //repőe matriz projecçăo
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glutPostRedisplay();
}


int picking(int x, int z){
	int i, n, objid = 0;
	double zmin = 10.0;
	GLuint buffer[100], *ptr;

	glSelectBuffer(100, buffer);
	glRenderMode(GL_SELECT);
	glInitNames();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix(); // guarda a projecçăo
	glLoadIdentity();
	setProjection(x, z, GL_TRUE);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	setCamera(estado.camera, modelo.objecto);
	desenhaEixos();

	n = glRenderMode(GL_RENDER);
	if (n > 0)
	{
		ptr = buffer;
		for (i = 0; i < n; i++)
		{
			if (zmin >(double) ptr[1] / UINT_MAX) {
				zmin = (double)ptr[1] / UINT_MAX;
				objid = ptr[3];
			}
			ptr += 3 + ptr[0]; // ptr[0] contem o número de nomes (normalmente 1); 3 corresponde a numnomes, zmin e zmax
		}
	}


	glMatrixMode(GL_PROJECTION); //repőe matriz projecçăo
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);

	return objid;
}

void mouse(int btn, int state, int x, int z){
	switch (btn) {
	case GLUT_RIGHT_BUTTON:
		if (state == GLUT_DOWN){
			estado.xMouse = x;
			estado.zMouse = z;
			if (glutGetModifiers() & GLUT_ACTIVE_CTRL)
				glutMotionFunc(motionZoom);
			else
				glutMotionFunc(motionRotate);
			cout << "Left down\n";
		}
		else{
			glutMotionFunc(NULL);
			cout << "Left up\n";
		}
		break;
	case GLUT_LEFT_BUTTON:
		if (state == GLUT_DOWN){
			estado.eixoTranslaccao = picking(x, z);
			if (estado.eixoTranslaccao)
				glutMotionFunc(motionDrag);
			cout << "Right down - objecto:" << estado.eixoTranslaccao << endl;
		}
		else{
			if (estado.eixoTranslaccao != 0) {
				estado.camera.center[0] = estado.eixo[0];
				estado.camera.center[1] = estado.eixo[1];
				estado.camera.center[2] = estado.eixo[2];
				glutMotionFunc(NULL);
				estado.eixoTranslaccao = 0;
				glutPostRedisplay();
			}
			cout << "Right up\n";
		}
		break;
	}
}


void redisplayAll(){
	display();
	drawMinimapSubwindow();

}

void initModels(){
	for (int i = 0; i < 10; i++){
		mdlviewer_init("models\\penguin.mdl", enemyModels[i]);
	}
	srand(time(0));

	int tamanho = 20;

	int quartoactual = m->quartoActual;
	Quarto &quarto = m->quartos[quartoactual];
	for (int i = 0; i <20; i++)
	{

		int x=0;
		int y = 0;
		int matriz[200][200];
		do{
			x = rand() % (201 - 0 + 1) + 0;
			
			y = rand() % (201 - 0 + 1) + 0;
			

		} while (detetaColisao(x, y, 0) && matriz[x][y]==1);
			matriz[(int)x][(int)y] = 1;
			x -= 100;
			y -= 100;
			int n = rand() % (20 - 1 + 1) + 1;
			int l = rand() % (3 - 1 + 1) + 1;

			/*mdlviewer_init("models\\barrel_wooden.mdl", obstacleModels[i]);
			quarto.obstaculos[i].altura = l;
			quarto.obstaculos[i].comprimento = l;
			quarto.obstaculos[i].largura = l;
			quarto.obstaculos[i].x = x;
			quarto.obstaculos[i].y = y;
			quarto.obstaculos[i].z = 0;
			*/
			switch (n){
				case 1:
					mdlviewer_init("models\\skeleton.mdl", obstacleModels[i]);
					quarto.obstaculos[i].altura =1;
					quarto.obstaculos[i].comprimento = 1;
					quarto.obstaculos[i].largura = 3;
					quarto.obstaculos[i].x = x;
					quarto.obstaculos[i].y = y;
					quarto.obstaculos[i].z = 0;
					
					break;
				case 2:
					mdlviewer_init("models\\desk_office-mahg.mdl", obstacleModels[i]);
					quarto.obstaculos[i].altura =1;
					quarto.obstaculos[i].comprimento = 2;
					quarto.obstaculos[i].largura = 1;
					quarto.obstaculos[i].x = x;
					quarto.obstaculos[i].y = y;
					quarto.obstaculos[i].z = 0 ;
					
					break;
				case 3:
					mdlviewer_init("models\\ronny.mdl", obstacleModels[i]);
					quarto.obstaculos[i].altura = l;
					quarto.obstaculos[i].comprimento = l;
					quarto.obstaculos[i].largura = l;
					quarto.obstaculos[i].x = x;
					quarto.obstaculos[i].y = y;
					quarto.obstaculos[i].z = 0;
					break;

				case 4:
					mdlviewer_init("models\\barrel_metal.mdl", obstacleModels[i]);
					quarto.obstaculos[i].altura = l;
					quarto.obstaculos[i].comprimento = l;
					quarto.obstaculos[i].largura = l;
					quarto.obstaculos[i].x = x;
					quarto.obstaculos[i].y = y;
					quarto.obstaculos[i].z = 0;
				
					break;
				case 5:
					mdlviewer_init("models\\trash.mdl", obstacleModels[i]);
					quarto.obstaculos[i].altura = 1;
					quarto.obstaculos[i].comprimento = 2;
					quarto.obstaculos[i].largura = 1;
					quarto.obstaculos[i].x = x;
					quarto.obstaculos[i].y = y;
					quarto.obstaculos[i].z = 0;

					break;
				
				case 6:
					mdlviewer_init("models\\barrel_brown.mdl", obstacleModels[i]);
					quarto.obstaculos[i].altura = 1;
					quarto.obstaculos[i].comprimento = 1;
					quarto.obstaculos[i].largura = 1;
					quarto.obstaculos[i].x = x;
					quarto.obstaculos[i].y = y;
					quarto.obstaculos[i].z = 0;

					break;
				case 7:
					mdlviewer_init("models\\bucket.mdl", obstacleModels[i]);
					quarto.obstaculos[i].altura = 1;
					quarto.obstaculos[i].comprimento = 1;
					quarto.obstaculos[i].largura = 1;
					quarto.obstaculos[i].x = x;
					quarto.obstaculos[i].y = y;
					quarto.obstaculos[i].z = 0;

					break;
				case 8:
					mdlviewer_init("models\\bag_food.mdl", obstacleModels[i]);
					quarto.obstaculos[i].altura = 1;
					quarto.obstaculos[i].comprimento = 1;
					quarto.obstaculos[i].largura = 1;
					quarto.obstaculos[i].x = x;
					quarto.obstaculos[i].y = y;
					quarto.obstaculos[i].z = 0;

					break;
				case 9:
					mdlviewer_init("models\\washingmachine.mdl", obstacleModels[i]);
					quarto.obstaculos[i].altura = 1;
					quarto.obstaculos[i].comprimento = 1;
					quarto.obstaculos[i].largura = 1;
					quarto.obstaculos[i].x = x;
					quarto.obstaculos[i].y = y;
					quarto.obstaculos[i].z = 0;

					break;


				default:
					mdlviewer_init("models\\barrel_wooden.mdl", obstacleModels[i]);
					quarto.obstaculos[i].altura = l;
					quarto.obstaculos[i].comprimento = l;
					quarto.obstaculos[i].largura = l;
					quarto.obstaculos[i].x = x;
					quarto.obstaculos[i].y = y;
					quarto.obstaculos[i].z = 0;
					
					break;
			}i;
	}
	quarto.numObstaculos = tamanho;
}

/*
audio init
sons tirados de https://www.freesound.org
*/
void InitAudio(){
	
/*	estado.buffer = alutCreateBufferFromFile("sounds/teste.wav");
	//estado.buffer[1] = alutCreateBufferFromFile("sounds/Footsteps_on_Cement.wav");
	alGenSources(1, &estado.source);
	alSourcei(estado.source, AL_BUFFER, estado.buffer);
	*/
	//create sound buffers
	estado.buffer[0] = alutCreateBufferFromFile("sounds/birds.wav");
	estado.buffer[1] = alutCreateBufferFromFile("sounds/concrete-step1.wav");
	estado.buffer[2] = alutCreateBufferFromFile("sounds/rain-01.wav");
	
	//create sound sources
	alGenSources(3, estado.source);
	
	//bind sound sources to buffers
	alSourcei(estado.source[0], AL_BUFFER, estado.buffer[0]);
	alSourcei(estado.source[1], AL_BUFFER, estado.buffer[1]);
	alSourcei(estado.source[2], AL_BUFFER, estado.buffer[2]);
}

/*
void guardaMundo(string stringToSplit){
	string delimiter = ",";
	size_t pos2 = 0;
	string token2;
	//cout << totalMundos << std::endl;
	token2 = stringToSplit.substr(0, pos2);
	//cout << token2 << std::endl;
	listaMundos[totalMundos].id = stoi(token2);
	stringToSplit.erase(0, pos2 + delimiter2.length());
	listaMundos[totalMundos].nome = stringToSplit;
	//cout << stringToSplit << std::endl;
	totalMundos++;
}*/

void splitMundos(string inputStringMapas){

	string delimiter1 = ";";
	string delimiter2 = ",";
	size_t pos1 = 0;
	string token;
	while ((pos1 = inputStringMapas.find(delimiter1)) != string::npos) {
		token = inputStringMapas.substr(0, pos1);
		size_t pos2 = 0;
		string token2;
		while ((pos2 = token.find(delimiter2)) != string::npos) {
			token2 = token.substr(0, pos2);
			listaMundos[totalMundos].id = stoi(token2);
			token.erase(0, pos2 + delimiter2.length());
			listaMundos[totalMundos].nome = token;
			totalMundos++;
		}
		
		inputStringMapas.erase(0, pos1 + delimiter1.length());
	}
}

void selecionaMundo(){
	string id_mundo_txt;
	cout << "==============================================" << endl;
	cout << "\t\tLista de Mundos" << endl;
	cout << "==============================================" << endl;
	cout << "----------------------------------------------" << endl;
	for (int i = 0; i < totalMundos; i++){
		cout << "\t" << listaMundos[i].id << " - " << listaMundos[i].nome << endl;
	}
	cout << "----------------------------------------------" << endl;
	cout << "Introduza o número do mundo que deseja jogar ";
	getline(std::cin, id_mundo_txt);
	dl_mundoId = stoi(id_mundo_txt);
}

void main(int argc, char **argv)
{
	int result_login, result_dl, result_ul;
	//dl_mundoId = 3;
	pisa_alcapao = false;

	nws = new WebServiceProxy();
	// Forçar loggin correcto
	modoJogo = LOGIN;
	do{
		result_login = nws->login();
	} while (0 != result_login);
	// Fazer upload do score do mapa actual
	result_ul = nws->uploadScore(modelo.pontos, dl_mundoId);
	if (result_ul = 0){
		cout << "vai entrar" << endl;
		nws->uploadPercurso(modelo.percurso, dl_mundoId);
	}
	//printf("%d", result_ul);
	
	nws->listMaps();

	#define __GRAFO__FILE__ "output.txt"
	#define __GRAFO__FILE__TESTE "outputTeste.txt"
	#define __GRAFO__FILE_MAPS "mapList.txt"
	//----------------------------------------
	// Ler mapas totais e guardar em memória
	//----------------------------------------
	ifstream maplistfile;
	maplistfile.open(__GRAFO__FILE_MAPS, ios::in);
	if (!maplistfile.is_open()) {
		cout << "Erro ao abrir " << __GRAFO__FILE_MAPS << "para ler" << endl;
		exit(1);
	}
	std::string inputStringMapas((std::istreambuf_iterator<char>(maplistfile)), std::istreambuf_iterator<char>());
	maplistfile.close();
	splitMundos(inputStringMapas);
	modoJogo = MENU_PRINCIPAL;
	selecionaMundo();
	// Depois de escolher o mapa
	result_dl = nws->downloadMap(dl_mundoId);

	//----------------------------------------
	// Ler grafo e guardar em memória
	//----------------------------------------
	ifstream myfile;
	myfile.open(__GRAFO__FILE__, ios::in);




	if (!myfile.is_open()) {
		cout << "Erro ao abrir " << __GRAFO__FILE__ << "para ler" << endl;
		exit(1);
	}
	

	std::string inputString((std::istreambuf_iterator<char>(myfile)), std::istreambuf_iterator<char>());
	myfile.close();	
	

	initTexturas(texID);
	m = new Mundo(inputString, &texID[ID_TEXTURA_PAREDES], dl_mundoId);
	glutInit(&argc, argv);

	// need both double buffering and z buffer 
	
	//Activar o blend para o alpha
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(800, 400);
	mainWindow= glutCreateWindow("Maze");
	
	glutReshapeFunc(myReshape);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(Special);
	glutSpecialUpFunc(SpecialKeyUp);
	mdlviewer_init("models\\homer.mdl", modelo.personagem);
	glutDisplayFunc(display);
	glutMouseFunc(mouse);
	glutTimerFunc(estado.timer, Timer, 0);
	glutIdleFunc(redisplayAll);

	myInit();
	
	alutInit(&argc, argv);
	InitAudio();

	imprime_ajuda();

	miniMapWindow = glutCreateSubWindow(mainWindow, 600, 0, 150, 80);
	myInit();
	glutReshapeFunc(reshapeMinimapSubwindow);
	glutDisplayFunc(drawMinimapSubwindow);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(Special);
	glutSpecialUpFunc(SpecialKeyUp);
	glutMouseFunc(mouse);
	glutTimerFunc(estado.timer, Timer, 0);

	mdlviewer_init("models\\penguin.mdl", enemyModel);

	//mdlviewer_init("models\\barrel_wooden.mdl", objectModel);
	
	initModels();
	modoJogo = JOGO;
	glutMainLoop();
	system("pause");
}