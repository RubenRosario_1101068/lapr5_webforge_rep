#pragma once
#include <WebServices.h>
#include <iostream>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>  
#include "schemas.microsoft.com.2003.10.Serialization.xsd.h"
#include "tempuri.org.wsdl.h"
#include "tempuri.org.xsd.h"
#include "stdafx.h"

using namespace std;

class WebServiceProxy {
	HRESULT	hr;
	WS_ERROR* error;
	WS_HEAP* heap;
	WS_SERVICE_PROXY* proxy;
	WS_ENDPOINT_ADDRESS	address;
	WS_STRING url;
	WS_HTTP_BINDING_TEMPLATE templ;

	string loginName;
	string password;

public:
	WebServiceProxy();
	~WebServiceProxy();
	void set_values(int, int);
	int login();
	int downloadMap(int map_id);
	int uploadScore(int score, int map_id);
	void listMaps();
	int uploadPercurso(string percurso, int map_id);
};