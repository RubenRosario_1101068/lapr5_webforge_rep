Bem vindos ao nosso repositório do projecto que desenvolvemos no âmbito de LAPR5!

A nossa equipa:

* Diogo Lima

* Duarte Pina

* Joana Verde

* Pedro Moura

* Pedro Valério 

* Ruben Nunes

* Ruben Rosario

A nossa documentação encontra-se na [Wiki](https://bitbucket.org/RubenRosario_1101068/lapr5_webforge_rep/wiki), nos issues respectivos e também no repositório.

O nosso site:
[WalkTheMaze](http://wvm134.dei.isep.ipp.pt/WalkTheMaze)