﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WalkTheMaze.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = @WalkTheMazeResources.Resource.DescPage;

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message =  @WalkTheMazeResources.Resource.ContactPage;

            return View();
        }
    }
}
