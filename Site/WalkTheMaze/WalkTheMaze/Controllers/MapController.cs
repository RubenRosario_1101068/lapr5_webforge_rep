﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WalkTheMaze.Models;
using WalkTheMaze.DAL;

namespace WalkTheMaze.Controllers
{
    public class MapController : Controller
    {
        private GameContext db = new GameContext();

        //
        // GET: /Map/
        [Authorize]
        public ActionResult Index()
        {
            if (User.Identity.Name == "admin")
            {
                return View(db.Maps.ToList());
            }
            else
            {
                //var maps = db.Maps.Where(m => m.User.UserName == user || m.Public).Include(m => m.User);
                //var maps = db.Maps.Where(m => m.MapType.Public);
                var maps = db.Maps.Where(m => m.MapType == MapType.Public);
                return View(maps.ToList());
                

            }
        }

        //
        // GET: /Map/Details/5
        [Authorize]
        public ActionResult Details(int id = 0)
        {
            Map map = db.Maps.Find(id);
            if (map == null)
            {
                return HttpNotFound();
            }
            return View(map);
        }

        //
        // GET: /Map/Create
        [Authorize(Users = "admin")]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Map/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Map map)
        {
            if (ModelState.IsValid)
            {
                db.Maps.Add(map);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(map);
        }

        //
        // GET: /Map/Edit/5
        [Authorize(Users = "admin")]
        public ActionResult Edit(int id = 0)
        {
            Map map = db.Maps.Find(id);
            if (map == null)
            {
                return HttpNotFound();
            }
            return View(map);
        }

        //
        // POST: /Map/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Map map)
        {
            if (ModelState.IsValid)
            {
                db.Entry(map).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(map);
        }

        //
        // GET: /Map/Delete/5
        [Authorize(Users = "admin")]
        public ActionResult Delete(int id = 0)
        {
            Map map = db.Maps.Find(id);
            if (map == null)
            {
                return HttpNotFound();
            }
            return View(map);
        }

        //
        // POST: /Map/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Map map = db.Maps.Find(id);
            db.Maps.Remove(map);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}