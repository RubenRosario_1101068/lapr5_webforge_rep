﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WalkTheMaze.Models;
using WalkTheMaze.DAL;

namespace WalkTheMaze.Controllers
{
    public class ScoreController : Controller
    {
        private GameContext db = new GameContext();

        //
        // GET: /Score/
        [Authorize]
        public ActionResult Index(int id = -1)
        {
            Map mapToView;
            List<Score> scores;

            if (id == -1) {
                mapToView = db.Maps.First();
            }
            else {
                mapToView = db.Maps.Find(id);
            }

            if (mapToView == null)
                return HttpNotFound();
            else
                scores = db.Scores.Include(s => s.Player)
                    .Include(s => s.Map)
                    .Where(m => m.MapID == mapToView.MapID)
                    .OrderByDescending(s => s.Points)
                    .ToList();
            ViewBag.maps = db.Maps.ToList();
            ViewBag.mapName = mapToView.MapName;
            return View(scores.ToList());
        }

        //
        // GET: /Score/Details/5
        [Authorize(Users = "admin")]
        public ActionResult Details(int id = 0)
        {
            Score score = db.Scores.Find(id);
            if (score == null)
            {
                return HttpNotFound();
            }
            return View(score);
        }

        //
        // GET: /Score/Create
        [Authorize(Users = "admin")]
        public ActionResult Create()
        {
            ViewBag.PlayerID = new SelectList(db.Players, "PlayerID", "Name");
            ViewBag.MapID = new SelectList(db.Maps, "MapID", "MapData");
            return View();
        }

        //
        // POST: /Score/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Score score)
        {
            if (ModelState.IsValid)
            {
                db.Scores.Add(score);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PlayerID = new SelectList(db.Players, "PlayerID", "Name", score.PlayerID);
            ViewBag.MapID = new SelectList(db.Maps, "MapID", "MapData", score.MapID);
            return View(score);
        }

        //
        // GET: /Score/Edit/5
        [Authorize(Users = "admin")]
        public ActionResult Edit(int id = 0)
        {
            Score score = db.Scores.Find(id);
            if (score == null)
            {
                return HttpNotFound();
            }
            ViewBag.PlayerID = new SelectList(db.Players, "PlayerID", "Name", score.PlayerID);
            ViewBag.MapID = new SelectList(db.Maps, "MapID", "MapData", score.MapID);
            return View(score);
        }

        //
        // POST: /Score/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Score score)
        {
            if (ModelState.IsValid)
            {
                db.Entry(score).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PlayerID = new SelectList(db.Players, "PlayerID", "Name", score.PlayerID);
            ViewBag.MapID = new SelectList(db.Maps, "MapID", "MapData", score.MapID);
            return View(score);
        }

        //
        // GET: /Score/Delete/5
        [Authorize(Users = "admin")]
        public ActionResult Delete(int id = 0)
        {
            Score score = db.Scores.Find(id);
            if (score == null)
            {
                return HttpNotFound();
            }
            return View(score);
        }

        //
        // POST: /Score/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Score score = db.Scores.Find(id);
            db.Scores.Remove(score);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}