﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WalkTheMaze.Models;

namespace WalkTheMaze.DAL
{
    public class GameContext : DbContext
    {

        public GameContext()
            : base("DefaultConnection")
        {

        }
        public DbSet<Player> Players { get; set; }
        public DbSet<Map> Maps { get; set; }
        public DbSet<Score> Scores { get; set; }
    }
}