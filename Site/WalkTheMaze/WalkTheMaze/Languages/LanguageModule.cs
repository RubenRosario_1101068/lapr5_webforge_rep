﻿//Language Module 
//Adaptacao do ficheiro extraido da pagina consultada para este issue, ver documentacao consultada do issue #60.

using System;
using System.Web;
using System.Threading;

/// <summary>
/// Summary description for LanguageModule
/// </summary>
public class LanguageModule : IHttpModule
{

    public void Init(HttpApplication context)
    {
        context.AcquireRequestState += new EventHandler(OnAcquireRequestState);
    }

    public void Dispose()
    {

    }

    public void OnAcquireRequestState(Object i_object, EventArgs i_eventArgs)
    {
        HttpApplication l_httpApplication = i_object as HttpApplication;

        // check whether the language change parameter has been passed
        var l_language =
            l_httpApplication.Request.Params["language"];
        var l_boolLanguageChanged = false;
        if (HttpContext.Current.Session != null && l_language == null)
        {
            // if language parameter is not sent, then take language from session
            l_language = (string)l_httpApplication.Session["language"];
        }
        else
        {
            // If language parameter is indeed sent, then user wants to change language.
            // I will make sure I tag this in order to redirect to.
            l_boolLanguageChanged = true;
        }

        if (l_language != null && HttpContext.Current.Session != null)
        {
            // having the language a thand, let us set it.
            var l_culture = new System.Globalization.CultureInfo(l_language);

            Thread.CurrentThread.CurrentCulture = l_culture;
            Thread.CurrentThread.CurrentUICulture = l_culture;

            // save language to session
            l_httpApplication.Session["language"] = l_language;

            // check whether I have redirect
            if (l_boolLanguageChanged)
            {
                if (l_httpApplication.Request.UrlReferrer != null)
                    l_httpApplication.Response.Redirect(
                       l_httpApplication.Request.UrlReferrer.AbsolutePath);
                else if (l_httpApplication.Request.Url != null)
                    l_httpApplication.Response.Redirect(
                   l_httpApplication.Request.Url.AbsolutePath);
            }
        }



    } // OnAcquireRequestState
    //-------------------------

} // class LanguageModule
//----