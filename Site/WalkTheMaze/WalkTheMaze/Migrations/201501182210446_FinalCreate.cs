namespace WalkTheMaze.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FinalCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Maps",
                c => new
                    {
                        MapID = c.Int(nullable: false, identity: true),
                        MapData = c.String(),
                        MapName = c.String(),
                        MapType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MapID);
            
            CreateTable(
                "dbo.Players",
                c => new
                    {
                        PlayerID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.PlayerID);
            
            CreateTable(
                "dbo.Scores",
                c => new
                    {
                        ScoreID = c.Int(nullable: false, identity: true),
                        Points = c.Int(nullable: false),
                        Data = c.DateTime(nullable: false),
                        MapID = c.Int(nullable: false),
                        PlayerID = c.Int(nullable: false),
                        Track = c.String(),
                    })
                .PrimaryKey(t => t.ScoreID)
                .ForeignKey("dbo.Maps", t => t.MapID, cascadeDelete: true)
                .ForeignKey("dbo.Players", t => t.PlayerID, cascadeDelete: true)
                .Index(t => t.MapID)
                .Index(t => t.PlayerID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Scores", "PlayerID", "dbo.Players");
            DropForeignKey("dbo.Scores", "MapID", "dbo.Maps");
            DropIndex("dbo.Scores", new[] { "PlayerID" });
            DropIndex("dbo.Scores", new[] { "MapID" });
            DropTable("dbo.Scores");
            DropTable("dbo.Players");
            DropTable("dbo.Maps");
        }
    }
}
