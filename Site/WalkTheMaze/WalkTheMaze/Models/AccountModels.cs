﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Security.AccessControl;
using System.Web.Security;

namespace WalkTheMaze.Models
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "UserName", ResourceType=typeof(WalkTheMazeResources.Resource))]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "CurrentPassword", ResourceType = typeof(WalkTheMazeResources.Resource))]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceName = "ErrorMinLength", MinimumLength = 6, ErrorMessageResourceType = typeof(WalkTheMazeResources.Resource))]
        [DataType(DataType.Password)]
        [Display(Name = "NewPassword", ResourceType = typeof(WalkTheMazeResources.Resource))]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "ConfirmPwd", ResourceType = typeof(WalkTheMazeResources.Resource))]
        [Compare("NewPassword", ErrorMessageResourceName = "ErrorPwdMatch", ErrorMessageResourceType = typeof(WalkTheMazeResources.Resource))]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "UserName", ResourceType = typeof(WalkTheMazeResources.Resource))]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(WalkTheMazeResources.Resource))]
        public string Password { get; set; }

        [Display(Name = "RememberMe", ResourceType = typeof(WalkTheMazeResources.Resource))]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "UserName", ResourceType = typeof(WalkTheMazeResources.Resource))]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceName = "ErrorMinLength", ErrorMessageResourceType = typeof(WalkTheMazeResources.Resource), MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(WalkTheMazeResources.Resource))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "ConfirmPwd", ResourceType = typeof(WalkTheMazeResources.Resource))]
        [Compare("Password", ErrorMessageResourceName = "ErrorPwdMatch", ErrorMessageResourceType = typeof(WalkTheMazeResources.Resource))]
        public string ConfirmPassword { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
