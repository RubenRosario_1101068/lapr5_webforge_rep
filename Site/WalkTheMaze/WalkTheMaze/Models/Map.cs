﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WalkTheMaze.Models
{

    public enum MapType
    {
        Public, Private
    }

    public class Map
    {
        public int MapID { get; set; } 
        public string MapData { get; set; } 
        public string MapName { get; set; } 
        public MapType MapType { get; set; }
    }
}