﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WalkTheMaze.Models
{
    public class Score
    {

        public int ScoreID { get; set; } 
        public int Points { get; set; }
        public DateTime Data { get; set; } 
        public int MapID { get; set; } 
        public int PlayerID { get; set; }
        public string Track { get; set; }


        public virtual Player Player { get; set; }
        public virtual Map Map { get; set; }
    }
}