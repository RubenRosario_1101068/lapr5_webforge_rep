﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WalkTheMaze.WebService {
    [ServiceContract]
    public interface IWebService {
        [OperationContract]
        int Login(string userName, string password);

        [OperationContract]
        int UploadScore(string userName, string password, int score, int mapID);

        [OperationContract]
        string DownloadMap(string userName, string password, int mapID);

        [OperationContract]
        string ListMaps(string userName, string password);

        [OperationContract]
        byte[][] DownloadTexturas(string userName, string password, int mapID);

        [OperationContract]
        byte[] DownloadModelo(string userName, string password, int mapID);

        [OperationContract]
        byte[] DownloadSons(string userName, string password, int mapID);

        [OperationContract]
        int UploadPercurso(string userName, string password, string percurso, int mapID);
    }
}
