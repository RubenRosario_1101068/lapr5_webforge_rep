﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;
using WalkTheMaze.Controllers;
using WalkTheMaze.DAL;
using WalkTheMaze.Models;
using WebMatrix.WebData;
using System.Data.Entity;


namespace WalkTheMaze.WebService {

    public class WebService : IWebService {

        private GameContext db = new GameContext();


        // Método do webservice para tentar autenticar utilizador
        // Parâmetros:
        //      userName - string com o login do utilizador no website
        //      password - password respectiva para o login
        // Valores de retorno:
        //      0 - credenciais válidas
        //      1 - credenciais inválidas
        public int Login(string userName, string password) {
            if (authenticateUser(userName, password)) {
                return 0;
            }
            return 1;
        }

        // Função para fazer o upload(criação/edição) de um score
        // Parâmetros:
        //      userName - string com o login do utilizador no website
        //      password - password respectiva para o login
        //      score - pontuação do utilizador
        //      mapID - Id do mapa referente ao score
        // Valores de retorno:
        //      0 - credenciais válidas
        //      1 - credenciais inválidas
        
        public int UploadScore(string userName, string password, int score, int mapID) {
            if (authenticateUser(userName, password))
            {
               
                Score newscore = new Score();
                Player player = new Player();

                player = db.Players.SingleOrDefault(p => p.Name == userName);
                if (player == null) 
                    return 1;


                newscore = db.Scores.SingleOrDefault(
                    s => s.PlayerID == player.PlayerID
                    && s.MapID == mapID);

                if (newscore == null)
                {
                    newscore = new Score();
                    newscore.PlayerID = player.PlayerID;
                    newscore.Points = score;
                    newscore.MapID = mapID;
                    newscore.Data = DateTime.Now;
                    db.Scores.Add(newscore); 
                }
                else
                {
                    
                    if (newscore.Points < score)
                    {
                        newscore.Points = score;
                        newscore.Data = DateTime.Now;
                        db.Entry(newscore).State = EntityState.Modified; 
                    }
                }
                db.SaveChanges();
                return 0;
            }
            return 1;
        }

        // Função para fazer o download de um mapa para a aplicação
        // Parâmetros:
        //      userName - string com o login do utilizador no website
        //      password - password respectiva para o login
        //      mapID - Id do mapa que se pretende fazer download
        // Valores de retorno:
        //      string - string com dados referentes ao mapa
        //      "" - Mapa Inexistente/ID incorrecto
        
        public string DownloadMap(string userName, string password, int mapID) {
            //return "Jose"; // Teste de conexão ao C++
            if (authenticateUser(userName, password))
            {

                Map map = db.Maps.SingleOrDefault(p => p.MapID == mapID);
                if (map != null)
                {
                    return map.MapData.ToString();
                }

            }   
            
            return "";
        }


        // Função para fazer o download de texturas para o jogo
        // Parâmetros:
        //      userName - string com o login do utilizador no website
        //      password - password respectiva para o login
        //      mapID - Id do mapa que se pretende fazer download
        // Valores de retorno:
        //      ficheiro com as texturas

        public byte[][] DownloadTexturas(string userName, string password, int mapID)
        {
            //return "Jose"; // Teste de conexão ao C++
            if (authenticateUser(userName, password))
            {
                try
                {
                    List<byte[]> texts = new List<byte[]>();
                    FileStream fel = null;

                    //textura do chao
                    fel = File.Open("mapData/" + mapID + "/Chao.bmp", FileMode.Open, FileAccess.Read);
                    byte[] b1 = new byte[fel.Length];
                    fel.Read(b1, 0, (int)fel.Length);
                    fel.Close();
                    texts.Add(b1);

                    //textura 
                    fel = File.Open("mapData/" + mapID + "/Textura1.bmp", FileMode.Open, FileAccess.Read);
                    byte[] b2 = new byte[fel.Length];
                    fel.Read(b2, 0, (int)fel.Length);
                    fel.Close();
                    texts.Add(b2);



                    return texts.ToArray();
                }
                catch (FileNotFoundException ex)
                {
                    return null;
                }
            }

            return null;
        }


        // Função para fazer o download de modelo para o jogo
        // Parâmetros:
        //      userName - string com o login do utilizador no website
        //      password - password respectiva para o login
        //      mapID - Id do mapa que se pretende fazer download
        // Valores de retorno:
        //      ficheiro com o modelo

        public byte[] DownloadModelo(string userName, string password, int mapID)
        {
            //return "Jose"; // Teste de conexão ao C++
            if (authenticateUser(userName, password))
            {
                try
                {
                    FileStream fel = null;

                    //modelo
                    fel = File.Open("mapData/" + mapID + "/Model.mdl", FileMode.Open, FileAccess.Read);
                    byte[] b1 = new byte[fel.Length];
                    fel.Read(b1, 0, (int)fel.Length);
                    fel.Close();

                    return b1;
                }
                catch (FileNotFoundException ex)
                {
                    return null;
                }
            }

            return null;
        }

        // Função para fazer o download de som para o jogo
        // Parâmetros:
        //      userName - string com o login do utilizador no website
        //      password - password respectiva para o login
        //      mapID - Id do mapa que se pretende fazer download
        // Valores de retorno:
        //      ficheiro com som

        public byte[] DownloadSons(string userName, string password, int mapID)
        {
            //return "Jose"; // Teste de conexão ao C++
            if (authenticateUser(userName, password))
            {
                try
                {
                    FileStream fel = null;

                    // som
                    fel = File.Open("mapData/" + mapID + "/som.wav", FileMode.Open, FileAccess.Read);
                    byte[] b1 = new byte[fel.Length];
                    fel.Read(b1, 0, (int)fel.Length);
                    fel.Close();

                    return b1;
                }
                catch (FileNotFoundException ex)
                {
                    return null;
                }
            }

            return null;
        }


        // Função utilitária para testar as credenciais de um utilizador
        // Parâmetros:
        //      userName - string com o login do utilizador no website
        //      password - password respectiva para o login
        // Valores de retorno:
        //      true - credenciais válidas
        //      false - credenciais inválidas

        private bool authenticateUser(string userName, string password) {
            // Verificar credenciais do utilizador
            if (WebSecurity.Login(userName, password)) {
                return true;
            }
            return false;
        }

        // Função para listar os mapas para serem escolhidos na aplicação para efectuar o download
        // Parâmetros:
        //      userName - string com o login do utilizador no website
        //      password - password respectiva para o login
        // Valores de retorno:
        //      string - string com dados referentes ao mapa
        //      "" - Não existem mapas

        public string ListMaps(string userName, string password){
            if (authenticateUser(userName, password))
            {

                List<Map> tempmap = new List<Map>();
                tempmap = db.Maps.ToList();
                string maps = null;

                foreach (var map in tempmap)
                {

                    maps += map.MapID.ToString();
                    maps += ",";
                    maps += map.MapName;
                    maps += ";";
                }

                if(maps != null)
                return maps;

            }
            return "";
        }

        // Função para fazer o upload(criação) de um percurso
        // Parâmetros:
        //      userName - string com o login do utilizador no website
        //      password - password respectiva para o login
        //      percurso - string com as coordenadas do percurso
        //      mapID - Id do mapa referente ao score
        // Valores de retorno:
        //      0 - credenciais válidas
        //      1 - credenciais inválidas

        public int UploadPercurso(string userName, string password, string percurso, int mapID)
        {
            if (authenticateUser(userName, password))
            {

                Score newscore = new Score();
                Player player = new Player();

                player = db.Players.SingleOrDefault(p => p.Name == userName);
                if (player == null)
                    return 1;


                newscore = db.Scores.SingleOrDefault(
                    s => s.PlayerID == player.PlayerID
                    && s.MapID == mapID);

                if (newscore == null)
                {
                    return 1;
                }
                else
                {

                    newscore.Track = percurso;
                    db.Entry(newscore).State = EntityState.Modified;
                    
                }
                db.SaveChanges();
                return 0;
            }
            return 1;
        }

    }
}
