# Labirinto #

O Labirinto/Mundo é criado a partir de texto a representar a estrutura de um grafo (nós e arcos). Nós com a mesma altura que tenham um arco de ligação, representam uma parede ou muro, com altura diferente, representam uma rampa entre eles.
O Mundo é composto por vários quartos, que por sua vez são compostos por nós/arcos,mas também possui alçapões, saidas (para outros quartos),  inimigos e obstáculos.

Chega-se assim ao seguinte diagrama de classes:

![](https://bitbucket.org/RubenRosario_1101068/lapr5_webforge_rep/raw/7863e50e25d0e3fe8c6e315df8baded48f0392ee/Docs/Jogo/diagrama%20classes.png)

# Inimigos #

Inimigos são personagens que ao colider com o jogador fazem-lo perder vida e pontos. Decidiu-se optar por esta alternativa ao invés de simplesmente 'matar' o jogador, como é feito no caso dos alçapões.
Estes inimigos são entidades proprias, com posição, direcção, vida, animação.

# Minimap #

O Minimapa é o redesenhar do labirinto com uma perspectiva de cima, menos texturas e iluminação.

# Projecteis #

O jogador pode disparar projecteis com o 'Space', estes assumem a direcção do jogador.
Estes projecteis são também entidades proprias, com velocidade, posição e raio.
Ao colider com um inimigo, 'mata' esse inimigo. A posição dos projecteis é actualizada a partir do Mundo, por intermédio do Timer.
