# --09/12/14-- #
1. Análise do enunciado;
2. Divisão do projeto em sub-problemas;
3. Enumerar as dúvidas para serem esclarecidas na sessão de esclarecimentos;
4. Análise e Modelação do problema (draft);
5. Definição das issues (início);

# --10/12/14-- #
1. Definição das issues (continuação)
2. Modelação do problema;
3. Definição da arquitetura;
4. Criação da estrutura do web site e modelos MVC

# --11/12/14-- #
1. Elaboração de tabela SCRUM geral do projeto.
2. Criação das Issues.
Observação: 
-Issues da primeira demo Estado:major, restantes Estado:minor.
-Issues de modelação 3D: #1 a #46.
-Issues de web: #47 a #55.

# --12/12/14-- #
1. Atribuição e atualização de lista de issues do módulo web.
2. Análise e modelação da base do projeto referente ao módulo navegação e visualização 3D.