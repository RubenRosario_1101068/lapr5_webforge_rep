WebSite

[TOC]
# Descrição

O WebSite vai ser o ponto de introdução dos utilizadores ao jogo. Vai ser no WebSite que se podem registar, fazer download da aplicação e consultar as leaderboards.


# Casos de uso
 
Identificamos os seguintes casos de uso, alguns não efectuados directamente no site mas ligados a este via webservice.

![](https://bitbucket.org/RubenRosario_1101068/lapr5_webforge_rep/raw/5f901bb9b07bde86f43b23700a428061c6ca2c70/Docs/Website/Use%20cases.png)

# Modelo de dominio

O modelo de dominio que conseguimos inferir inicialmente, para o WebSite, foi o seguinte:
![](https://bitbucket.org/RubenRosario_1101068/lapr5_webforge_rep/raw/5f901bb9b07bde86f43b23700a428061c6ca2c70/Docs/Website/Modelo%20de%20dominio.png?at=master)

# Issues criados
Cada página de issue tem mais informação relacionada com o issue.

* [WebSite](https://bitbucket.org/RubenRosario_1101068/lapr5_webforge_rep/issue/47/website)
* [Login e Registo WebSite](https://bitbucket.org/RubenRosario_1101068/lapr5_webforge_rep/issue/50/)
* [Consultar Leaderboard no Website](https://bitbucket.org/RubenRosario_1101068/lapr5_webforge_rep/issue/49)
* [Login - Webservice](https://bitbucket.org/RubenRosario_1101068/lapr5_webforge_rep/issue/56)
* [Download de Mapas - Web Service](https://bitbucket.org/RubenRosario_1101068/lapr5_webforge_rep/issue/51)
* [Upload de pontuação - WebService](https://bitbucket.org/RubenRosario_1101068/lapr5_webforge_rep/issue/48)
* [Globalização/localização](https://bitbucket.org/RubenRosario_1101068/lapr5_webforge_rep/issue/58)
* [Logging dos erros e excepções](https://bitbucket.org/RubenRosario_1101068/lapr5_webforge_rep/issue/57)

